C Include File for UVJ
	
	implicit real*8 (a-h,o-z)
	implicit integer*8 (i-n)
c	inlcude 'mpif.h'

        integer*8   lxs,lys,ndimui,ndimdi,ndimi,icor,iz
	integer*8  lxy
	integer   ierr
	integer nsites

c        parameter (lxs= 4,lys=1,ndimui=6,   ndimdi=6,   ndimi=36)
c       parameter (lxs= 6,lys=1,ndimui=20,  ndimdi=20,  ndimi=400)
c        parameter (lxs= 8,lys=1,ndimui=70,  ndimdi=70,  ndimi=4900)
c       parameter (lxs= 9,lys=1,ndimui=126, ndimdi=126, ndimi=15876)
c       parameter (lxs=10,lys=1,ndimui=252, ndimdi=252, ndimi=63504)
c       parameter (lxs=12,lys=1,ndimui=924, ndimdi=924, ndimi=853776)
c       parameter (lxs=14,lys=1,ndimui=3432,ndimdi=3432,ndimi=11778624)
c       parameter (lxs=16,lys=1,ndimui=560 ,ndimdi=560 ,ndimi=313600)   ! 3+3
c       parameter (lxs=16,lys=1,ndimui=1820,ndimdi=1820,ndimi=3312400)  ! 4+4
c       parameter (lxs=16,lys=1,ndimui=4368,ndimdi=1820,ndimi=7949760)  ! 5+4
c         parameter (lxs=16,lys=1,ndimui=4368,ndimdi=4368,ndimi=19079424) ! 5+5

c	parameter (lxs=8,lys=2,ndimui=1820,ndimdi=1820,ndimi=3312400)  ! 4+4
c        parameter (lxs=8,lys=2,ndimui=4368,ndimdi=4368,ndimi=19079424) ! 5+5
c       parameter (lxs=8,lys=2,ndimui=4368,ndimdi=560,ndimi=2446080)   ! 5+3
c       parameter (lxs=8,lys=2,ndimui=8008,ndimdi=120,ndimi=960960)    ! 6+2
c       parameter (lxs=8,lys=2,ndimui=11440,ndimdi=16,ndimi=183040)    ! 7+1
c       parameter (lxs=8,lys=2,ndimui=8008,ndimdi=1820,ndimi=14574560) ! 6+4 
c       parameter (lxs=8,lys=2,ndimui=11440,ndimdi=560,ndimi=6406400)  ! 7+3
c       parameter (lxs=8,lys=2,ndimui=12870,ndimdi=120,ndimi=1544400)  ! 8+2
c       parameter (lxs=8,lys=2,ndimui=11440,ndimdi=16,ndimi=183040)    ! 9+1
c	parameter (lxs=4,lys=2,ndimui=70,ndimdi=70,ndimi=4900)         ! 4+4

c       parameter (lxs=15,lys=1,ndimui=1365,ndimdi=1365,ndimi=1863225)  ! 4+4
c       parameter (lxs=15,lys=1,ndimui=3003,ndimdi=3003,ndimi=9018009)  ! 5+5

c       parameter (lxs= 4,lys=4,ndimui=70,  ndimdi=70,  ndimi=4900)     ! 2D08

c       parameter (lxs= 3,lys=3,ndimui=126, ndimdi=126, ndimi=15876)    ! 3x3

c       parameter (lxs= 4,lys=4,ndimui=16  ,ndimdi=16  ,ndimi=256)      ! 2+2
c       parameter (lxs= 4,lys=4,ndimui=1820,ndimdi=1820,ndimi=3312400)  ! 4+4
c       parameter (lxs= 4,lys=4,ndimui=4368,ndimdi=560,ndimi=2446080)   ! 5+3
c       parameter (lxs= 4,lys=4,ndimui=8008,ndimdi=120,ndimi=960960)    ! 6+2
c       parameter (lxs= 4,lys=4,ndimui=11440,ndimdi=16,ndimi=183040)    ! 7+1
c        parameter (lxs= 4,lys=4,ndimui=4368,ndimdi=4368,ndimi=19079424) ! 5+5
c       parameter (lxs= 4,lys=4,ndimui=8008,ndimdi=1820,ndimi=14574560) ! 6+4 
c       parameter (lxs= 4,lys=4,ndimui=11440,ndimdi=560,ndimi=6406400)  ! 7+3
c       parameter (lxs= 4,lys=4,ndimui=12870,ndimdi=120,ndimi=1544400)  ! 8+2
c       parameter (lxs= 4,lys=4,ndimui=11440,ndimdi=16,ndimi=183040)    ! 9+1
c        parameter (lxs= 6,lys=6,ndimui=58905,ndimdi=58905,
c     1              ndimi= 3469799025)    ! 4+4 (36) (> 103g mem)
c        parameter (lxs= 6,lys=6,ndimui=376992,ndimdi=376992,
c     1              ndimi= 142122968064)    ! 5+5 (36) (> 103g mem)
c        parameter (lxs= 6,lys=6,ndimui=1947792,ndimdi=376992,
c     1              ndimi= 734302001664)    ! 6+5 (36) (> 21T mem)
c	 parameter (lxs= 4,lys=8,ndimui=906192,ndimdi=201376,
c     1              ndimi=182485320192)    ! 6+5 (32) (> 120 node)
c	 parameter (lxs= 4,lys=8,ndimui=906192,ndimdi=201376,
c     1              ndimi=182485320192)    ! 6+5 (32) (> 120 node)
c  	parameter (lxs= 4,lys=8,ndimui=35960,ndimdi=35960,
c     1              ndimi=1293121600)    ! 4+4 (32)
     parameter (lxs= 4,lys=8,ndimui=201376,ndimdi=201376,
    1              ndimi=40552293376)    ! 4+4 (32)

c	parameter (lxs= 4,lys=6,ndimui=735471,ndimdi=735471,
c     1              ndimi=540917591841)    ! 8+8 (24)
c	parameter (lxs= 4,lys=6,ndimui=134596,ndimdi=134596,
c     1              ndimi=18116083216)    ! 6+6 (24)
c	parameter (lxs= 4,lys=6,ndimui=10626,ndimdi=10626,
c     1              ndimi=112911876)    ! 4+4 (24)
c       parameter (lxs= 4,lys=4,ndimui=8008,ndimdi=4368,ndimi=)         ! 6+5
c       parameter (lxs= 4,lys=4,ndimui=8008,ndimdi=8008,ndimi=64128064) ! 6+6

C 3312400/112M; 19079424/605M;

        parameter (lxy=lxs*lys,max2=2**lxy,icor=12,iz=256)
!       parameter (lxy=lxs*lys,max2=2**lxy,icor=12,iz=784)
!       parameter (lxy=lxs*lys,max2=2**lxy,icor=12,iz=3136)

c	implicit real*8 (a-h,o-z)

	integer*8   time
	external  time
	integer*8   dataid,nnt,nntdw,nntup
	integer   iseed,iseedx
	integer	   nsize

C       dimension wv1(0:ndimi),wv2(0:ndimi),wz(ndimi),azdia(ndimi)

	dimension mcodeu(0:ndimui),mcoded(0:ndimdi)
c        dimension itmp1v(0:ndimui),itmp2v(0:ndimdi)
c        dimension iav(0:max2),ibv(0:max2),ncvec(max2+1)
        dimension nsign1(lxy,lxy)
        dimension idenv(lxy),nsub(lxy),nv(lxy,2),nvs(lxy,2)
	dimension isymv(lxy,8*lxy),isymbv(lxy,4*lxy)
        dimension jbv(8*lxy),jav(8*lxy),jrv(lxy),nwlv(lxy)
        dimension uint(lxy),epsi(lxy,2)
	dimension vint(8*lxy),ajxv(8*lxy),ajzv(8*lxy),hopt(8*lxy)

        dimension zhopv(0:1,0:1),zjv(0:1,0:1,0:1,0:1)
	dimension tmpva(0:1,0:1),tmpvb(0:1,0:1),itmpv(lxy)
	dimension tmpvc(0:1,0:1,0:1,0:1),tmpvd(0:1,0:1,0:1,0:1)

        dimension betav(iz),alphav(iz),trih(iz,iz),z(iz,iz)
        dimension d(iz),de(iz)

	parameter (icoeff=1)                ! parameter (icoeff=ndimui)
	dimension coeffM(icoeff,icoeff),coeffD(ndimui),
     &              coeffZ(icoeff,icoeff)

        dimension corv(lxy,lxy,icor)
	dimension scorz(lxy,lxy),scorx(lxy,lxy)
	dimension curv(lxy,lxy),bowv(lxy,lxy,2)
        dimension sigmac(lxy),ipxv(lxy),ipyv(lxy)
	dimension xv(lxy),yv(lxy),akxv(lxy),akyv(lxy),akv(lxy)
	dimension ixv(lxy),iyv(lxy),iposit(-lxs:lxs,-lxs:lys),
     &            kxv(lxy),kyv(lxy)
	dimension ipair(lxy,lxy),pair(lxy,lxy),pairz(lxy,lxy),
     &            paird(lxy),pairwz(lxy),pairm(lxy,lxy,lxy,lxy)

        dimension nwzi(99),nhist(51)

	complex*16 fk(lxy,icor),cxxx,czero,cone

	equivalence (corv(1,1,7),scorz(1,1)),(corv(1,1,8),scorx(1,1))
	equivalence (corv(1,1,9),bowv(1,1,1))
	equivalence (corv(1,1,11),curv(1,1)),(corv(1,1,12),pair(1,1))

c	character*24 idate
	character*8  idate
        character*6  datein,lattice_id,sigmac
        character*10 filea,fileb,swv(0:9),qwv(32)
	character*13 bclabel(0:2)
	character*14 coraw(icor)

        include 'mpif.h'

        common/nall/nsites,nup,ndown,ndim,ndimu,ndimd,ndimdm,numb,
     1              ncor,iden2,iorx,nsym,nsymb,nd12,ndiag,
     &              dataid,iseed,iseedx,nnt,nntdw,nntup
	common/size/nsize
	common/nerror/ierr
        common/svec/xv,yv,akxv,akyv,akv
        common/nvec/mcodeu,mcoded,iav,ibv,ncvec,jav,jbv,idenv,nsub,
     1              jrv,nwlv,nv,nsign1,isymv,isymbv,ipxv,ipyv,ipair,
     2              ixv,iyv,iposit,kxv,kyv,nwzi,nhist,nvs,itmp1v,itmp2v
        common/par/hopt,vint,uint,epsi,ajxv,ajzv,zhopv,zjv,anor,
     &             qxin,qyin,e0
        common/zos/z,trih,d,de,betav,alphav
	common/ave/corv,pix,piy
	common/sc/pairz,paird,pairwz,pairm
	common/ctl/ihdia,iszsz,isxsx,ibxy,ijxjx,ipxpx,
     &             iweig,isymch,idata,ibc
	common/misc/idate,datein,lattice_id,sigmac,bclabel,coraw,
     &              filea,fileb,swv,qwv
	common/temp/tmpva,tmpvb,tmpvc,tmpvd,itmpv
	common/ftr/fk,cxxx
	common/paru/uinput,vinput

        data zero/0.0D0/, one/1.0D0/, two/2.0D0/, four/4.0D0/
	data czero/(0.0D0,0.0D0)/, cone/(1.0D0,0.0D0)/

C Corv(i,j,1) = <N(i,+)N(j,+)>
C Corv(i,j,2) = <N(i,-)N(j,-)>
C Corv(i,j,3) = <2N(i,+)N(j,-)>
C Corv(i,j,4) = <Hole(i)Hole(j)>
C Corv(i,j,5) = <D(i,+)C(j,+)>
C Corv(i,j,6) = <D(i,-)C(j,-)>
C Corv(i,j,7) = <S(i,z)S(j,z)>
C Corv(i,j,8) = <S(i,x)S(j,x)>
C Corv(i,j,9) = <B(i,j,X)>
C Corv(i,j,10)= <B(i,j,Y)>
C Corv(i,j,11)= <C(i,j,X)>
C Corv(i,j,12)= <P(i,j,X)>

