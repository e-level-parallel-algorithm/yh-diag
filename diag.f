        PROGRAM UVJ
C Last Modification  Feb 18, 2006; for students
C
C Eigenvalues & Eigenvectors of Anderson Lattice Model  14-Mar-1992
C     Obtained by Lanczos Method
C Considering Bondings instead of Hoppings.  Dons_??
C Sub-Lattice Coding Methods,  No Symmetry Used
C
C MCODEU(NDIMU),MCODED(NDIMD):  NDIMU=N!/(N+)!(N - N+)!
c
C WV( ,2),WZ( ),AZDIZ( ): (NDIM);  NDIM = NDIMU * NDIMD
c
C IAV(),IBV(),NCVEC( +),NSIGNV(): (2**NSITES)   Temporary.
c
C IDENV(),NSUB(),NV( ,2),JRV(),NWLV(),UINT(),EPSI(),SIGMAC(): (NSITES)
c
C CORV(,),SCORZ(,),SCORX(,),NSIGN1(,): (NSITES,NSITES)
c
C NZEROV(0:1,0:1);  JBV(),JAV(): (NUMB)

C Change to CRAY
C 31,$ s/dfloat/float/g
C 31,$ s/dsqrt/sqrt/g
C 31,$ s/dcmplx/cmplx/g
C 31,$ s/dabs/abs/g
C 31,$ s/cdexp/cexp/g
C 31,$ s/stop/call exit/g
C 31,$ s/dreal/real/g
C 31,$ s/dimag/aimag/g
C delete SUB_second

        include 'uvj.h'

        real*8,allocatable::wv1(:),wv2(:),wz(:)
        integer nrank 

        call MPI_INIT(ierr)
        call MPI_COMM_SIZE(MPI_COMM_WORLD,nsize,ierr)
        call MPI_COMM_RANK(MPI_COMM_WORLD,nrank,ierr)


        if(nrank == 0) then
            call second(truns)
            call DATE_AND_TIME(idate)   ! depends on machine
C       call date(idate)
            dataid=time()

            call symbol

            write(6,*) 'Did You Turn Off J?'
            write(6,*) 'Enter ntimes, File: a,b'
            read(5,*) ntimes,filea,fileb
         endif

         call MPI_BCAST(ntimes,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr) 
         call MPI_BCAST(filea,2,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)
         call MPI_BCAST(fileb,2,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr) 
        
        
        
c        do 10000 nrun=1,ntimes

        if(nrank == 0) then
            write(6,*) 'Enter nsites,nd12,ndim,numb,nup,ndown'
            read(5,*) nsites
            read(5,*) nd12
            read(5,*) ndim
            read(5,*) numb
            read(5,*) nup
            read(5,*) ndown
c           if(nsites .ne. lxy .or. ndim .ne. ndimi) stop "Wrong Input"
        
        endif
        
c        write(6,*) nup,nsites,nrank
        call MPI_BCAST(nsites,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(nd12,  1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ndim,  1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(numb,  1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(nup,   1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ndown, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)

c        write(6,*) nup,nsites,nrank
        
        nnt = ndim / nsize
        nsid = mod(ndim,nsize)
c        if(nsid .ne. 0) then 
c           write(6,*)"the number of processor is wrong !"
c        endif
        

        if(nrank == 0 ) then 
             allocate( wv1(0:nnt),wv2(0:nnt) )
             allocate( wz(1:nnt))
        else 
             allocate( wv1(1:nnt),wv2(1:nnt),wz(1:nnt) )
        endif 


        if(nrank == 0) then
            write(6,*) 'Enter Iorder,Ndiag,Ngs,Ilancs,Ilancz'
            read(5,*) iorder
            read(5,*) ndiag
            read(5,*) ngs
            read(5,*) ilancs
            read(5,*) ilancz
        endif

        call MPI_BCAST(iorder, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ndiag,  1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ngs,    1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ilancs, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ilancz, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
            
        if(nrank == 0) then
            write(6,*) 'Ihdia,Iszsz,Isxsx,Ibxy,Ijxjx,Ipxpx',
     &             'Iweig,Isymch,Idata,Ibc'
            read(5,*) ihdia
            read(5,*) iszsz
            read(5,*) isxsx
            read(5,*) ibxy
            read(5,*) ijxjx
            read(5,*) ipxpx
            read(5,*) iweig
            read(5,*) isymch
            read(5,*) idata
            read(5,*) ibc
        endif
        
        call MPI_BCAST(ihdia, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(iszsz, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(isxsx, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ibxy,  1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ijxjx, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ipxpx, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(iweig, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(isymch,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(idata, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ibc,   1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)

        if(nrank == 0) then 
            write(6,1007) nsites,nd12,ndim,numb,nup,ndown,
     2      iorder,ndiag,ngs,ilancs,ilancz,iszsz,isxsx,ibxy,ipxpx,iweig
1007        format(1x,2i2,i9,i4,2i3,i7,i3,2x,3i2,2x,5i2/)
        

            write(6,1005) idate,dataid
1005        format(6x,a24,'   UVJ  ID_',i10/)
        endif

        ne=nup+ndown
        nsz=nup-ndown

        if(nrank == 0) then
            write(6,1002) nsites,nd12,numb,ne,nsz,bclabel(ibc)
1002        format(1x,i2,'-Site ',i1,'-D UVJ Model with ',i3,
     3      ' Bonds and Ne = ',i2,' Sz = ',i2,'/2',2x,a13/)

C                                               Set Necessary Indices
            write(6,*) 'Call Index.    Lattice = ',lattice_id
        endif

        call indexs(nrank)

        if(nrank == 0) then 
           if(idata .eq. 1) then
              write(6,*) 'Write All Data to File_dataall'
              open (unit=8,file='dataall',status='unknown')
              write(8,*) nsites,nd12,ndim,numb,nup,ndown
              write(8,*) iorder,ndiag,ngs,ilancs,ilancz
              write(8,*) ihdia,iszsz,isxsx,ibxy,ijxjx,iweig,isymch,
     &                   idata,ibc
              write(8,*) uint(1),vint(1),vint(numb),
     &                hopt(1),hopt(numb),epsi(1,1),epsi(1,2)
           end if
        endif


C                                               Set Hilbert Space
        if(nrank == 0) then 
           call second(tt1)
           write(6,*) 'Call Nstate'
        endif
 
c        write(6,'(1x,i2,1x,i2,1x,i8)') nup,nsites,nrank
c        write(6,*) nup,nsites,nrank
        call nstate(nrank)

        ndimd=ndim/ndimu
        ndimdm=ndimd-1


c      iorder .ne. 101  from input file
        if(iorder.eq.101) then
           write(6,*) 'Check Configurations'
           do 1 i=1,ndim
              indexd=(i-1)/ndimu
              indexu=i-ndimu*indexd
              ncodeu=mcodeu(indexu)
              ncoded=mcoded(indexd)
              ispin=1
              call decode(nv,nsites,ispin,ncodeu)
              ispin=2
              call decode(nv,nsites,ispin,ncoded)
              write(6,*) 'State_',i
              write(6,*) 'U: ',mcodeu(indexu),(nv(j,1),j=1,nsites)
              write(6,*) 'D: ',mcoded(indexd),(nv(j,2),j=1,nsites)
1          continue
           stop
        end if

        if(nrank == 0) then
             call second(tt2)
 
             write(6,*) 'Nstate. CPU = ',tt2-tt1
        endif

        if(ilancz .eq. 1) goto 499

C                                               AZDIA
C                                               May Not Use Azdia
        if(nrank == 0) then

           if(ihdia .eq. 1) then
               write(6,*) 'Making Vector AZDIA'
               call second(tt1)
           endif 
        endif

        
C                                            N.N, etc.
C******************************PARALLEL***************************

c********************* numb = nsites

c        do 121 ibond=1,numb
c           ib=jav(ibond)
c           jb=jbv(ibond)

C******************* idenv(j)=2**(j-1)
           
c           ideni=idenv(ib)
c           idenj=idenv(jb)
c           vq=vint(ibond)
c           ajzq=ajzv(ibond)/four
c           do 141 m=nrank*nnt+1,(nrank+1)*nnt
c              indexd=(m-1)/ndimu
c              indexu=m-ndimu*indexd
c              ncodeu=mcodeu(indexu)
c              ncoded=mcoded(indexd)
c              iup=iand(ideni,ncodeu)/ideni
c              idw=iand(ideni,ncoded)/ideni
c              jup=iand(idenj,ncodeu)/idenj
c              jdw=iand(idenj,ncoded)/idenj
c              azdia(m)=azdia(m)+vq*dfloat( (iup+idw)*(jup+jdw) )
c     &                         +ajzq*dfloat( (iup-idw)*(jup-jdw) )
c              
c              if(azdia(m) .ne. 0.0 .and. nrank .eq. 0) then
c                 write(6,*)"azdia(",m,") is not eq  0.0"
c              endif 
c
c141        continue
c121     continue

C********************************************************************
          if(ndiag .eq. 14 .or. ndiag .eq. 15) then
              open (unit=13,file='hm_v',form='unformatted',
     &              status='unknown')
              write(13) ndim
c              write(13) (azdia(m),m=1,ndim)
              close (unit=13)
c              do m=1,ndim
c                 azdia(m)=zero
c              end do
          end if

C*************************parallel***************************
C       					On-site

c        do 160 j=1,nsites
c           ideni=idenv(j)
c           uq=uint(j)
c           eq=epsi(j,1)
c           hq=epsi(j,2)
c           do 180 m=nrank*nnt+1,(nrank+1)*nnt
c              indexd=(m-1)/ndimu
c              indexu=m-ndimu*indexd
c              ncodeu=mcodeu(indexu)
c              ncoded=mcoded(indexd)
c              iup=iand(ideni,ncodeu)/ideni
c              idw=iand(ideni,ncoded)/ideni
c              azdia(m)=azdia(m)+uq*dfloat( iup*idw )
c     &                         +eq*dfloat( iup+idw )
c     &                         +hq*dfloat( iup-idw )
c180        continue
c160     continue


c**************************************************************

        if(ndiag .eq. 14 .or. ndiag .eq. 15) then
           open (unit=13,file='hm_u',form='unformatted',
     &               status='unknown')
           write(13) ndim
c           write(13) (azdia(m),m=1,ndim)
           close (unit=13)
c           do m=1,ndim
c           azdia(m)=zero
c           end do
        end if

c       iorder = 1
        iorx=mod(iorder,1000)

        if(nrank == 0) then
             call second(tt2)
              write(6,*) 'Azdia. CPU = ',tt2-tt1
        end if

C                                               Deal H-Matrix
c        ndiag .eq. 3 from input file
        if(ndiag.ge.10) then
           call second(tt1)
           write(6,*) 'Call Hmatrix. ndiag = ',ndiag
           if(ndim .gt. iz) then                ! need iz=ndim to write H
              write(6,*) 'Can not Do Hmatrix'
              stop
           end if
           call hmatx                 
           write(6,*) 'Eigenvector # ',iorx
           do 40 m=1,ndim
              wz(m)=z(m,iorx)
40         continue
           call second(tt2)
           write(6,*) 'Hmatx. CPU = ',tt2-tt1
           goto 999
        end if

        if(nrank == 0) then
            call second(tt1)
        end if

        if(ilancs .eq. 1) then
           write(6,*) 'Read in Output From Lancs'
           open(unit=11,file=filea,status='unknown')
           read(11,*) ndimr,ndimur
           if(ndimr.ne.ndim .or. ndimur.ne.ndimu) stop
           read(11,*) jp
           read(11,*) (alphav(i),i=1,jp)
           read(11,*) (betav(i),i=1,jp)
           do 60 j=1,jp
              read(11,*) (z(i,j),i=1,jp)
60         continue
           close (unit=11)
           read(5,*) init
           goto 9901
        end if

C                                               Set Initial State
        if(nrank ==0) then
           do 50 i=0,nnt
              wv1(i)=zero
              wv2(i)=zero
50      continue
        else 
c           do 51 i=nrank*nnt+1,(nrank+1)*nnt
           do 51 i=1,nnt
              wv1(i)=zero
              wv2(i)=zero
51      continue

        end if

        ndimx=ndim

c     the ndiag in the input file is 3,so ndimx == ndim
        if(ndimx .gt. iz .and. ndiag .eq. 1) ndimx=iz

        do 55 i=1,nnt
           wz(i)=zero
55      continue

        if(nrank == 0) then 
            write(6,*) 'Initial State'
            write(6,*) 'Enter # of Initial Configuration'
            read(5,*) init
            write(6,*) init
            write(6,*) 'Enter Electron Configuration & Coeffcient'
c           write(6,1001)
        endif


        call MPI_BCAST(init,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
       
        iseedx=0
        call initl(init,nrank,wv1(1))
        n1=1
        n2=2
        anor=dsqrt(anor)
        do 30 m=1,nnt
           wv1(m)=wv1(m)/anor
30      continue

c
C wv(m,2) = wv(k,1) * h(k,m) = h(m,k) * wv(k,1)
c
          
c       do i=nrank*nnt+1,(nrank+1)*nnt  
       do i=1,nnt  
         if( nrank*nnt+i == 3450) then
            wv1(i) = 1.0d0
         else
            wv1(i) = 0.0d0
         endif 
       enddo


        if(nrank == 0) then
            call second(tt3)
        end if
       
       call hop(n1,n2,nrank,wv1(1),wv2(1))
        if(nrank == 0) then
            call second(tt4)
        endif

        if(nrank == 0) then
            write(6,*) 'Hop. CPU = ',tt4-tt3
        endif 
        if(iorder.eq.301) stop

        if(nrank == 0) then
            write(6,*) 'Lanczos Procedure. 
     &                  Enter # of Steps (=< 70, > 100?)'
            read(5,*) nlanc
            if(nlanc .gt. iz) nlanc=iz
            write(6,*) 'Call Lanczos',ndiag,nlanc
        endif 

        call MPI_BCAST(nlanc,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)



C                                               Using Standard Lanczos
c        call lancs(nlanc,jp,nrank,wv1(nrank*nnt+1),wv2(nrank*nnt+1),
c     &                  wz(nrank*nnt+1)) 
        call lancs(nlanc,jp,nrank,wv1(1),wv2(1),wz(1)) 
    

        if(ilancs .eq. 2) then
           if(nrank == 0) then

           write(6,*) 'Write Output From Lancs'
           open(unit=11,file=filea,status='unknown')
           write(11,*) ndim,ndimu
           write(11,*) jp
           write(11,*) (alphav(i),i=1,jp)
           write(11,*) (betav(i),i=1,jp)
           do 56 j=1,jp
              write(11,*) (z(i,j),i=1,jp)
56         continue
           close (unit=11)

           endif
        end if
9901    continue

        if(nrank == 0) then 
            if(idata .eq. 1) write(8,*) d(1)
            call second(tt2)
            write(6,*) 'Lancs. CPU = ',tt2-tt1
        endif

C For Spectral Functions or Making Initial Wavefunctions

!        if(nsites .gt. 32) stop 'only have 32 qwv!'

        if(nrank == 0) then

           do j=1,nsites
              open (unit=2,file=qwv(j),status='unknown')
              write(2,*) nsites
              write(2,*) d(1)
              write(2,*) akxv(j),akyv(j)
              write(2,2002) ndouble,(jx,jx=0,nsites)
           end do
2002    format(i2,2x,36i2)

        endif 

        if(init/1000 .ne. 0) then
           if(nrank == 0) then
              write(6,*) 'Spectral Function Data in spect'
              open (unit=11,file='spect',status='unknown')
              write(11,*) nsites,uint(1),vint(1),nup,ndown
              write(11,*) e0,qxin,qyin
              anor=anor*anor
              write(11,*) anor,anor/3.141592654,' = <0|A^+A|0>'
              write(11,*) jp
           endif

           sum=zero

           do 58 j=1,jp
              sum=sum+z(1,j)*z(1,j)
              if( nrank == 0) then
                 write(11,*) d(j),d(j)-d(1),z(1,j)*z(1,j),sum
              endif
58         continue

           if(nrank == 0) then
              close (unit=11)
           endif
        end if

499     continue

        if(nrank == 0) then
           call second(tt1)
        endif 

        if(ilancz .eq. 1) then

           write(6,*) 'Read in Output From Lancz'
           open(unit=12,file=fileb,form='unformatted',status='unknown')
           read(12) ndimr,ndimur
           if(ndimr.ne.ndim .or. ndimur.ne.ndimu) stop
           read(12) (wz(m),m=1,ndim)
           close (unit=12)
           read(5,*) init
           call initl(init)
           goto 9902
        end if

        if(ndiag.ne.1) then
           nlanc=jp
          if(nrank == 0) then
           write(6,1001)
           write(6,*) 'Call Lanczos for Eigenvector',iorx,nlanc
           endif

c           do 100 i=nrank*nnt+1,(nrank+1)*nnt
           do 100 i=1,nnt
              wz(i)=zero
              wv1(i)=zero
              wv2(i)=zero
100        continue
           
           if(nrank == 0)then
           write(6,*) 'Reenter Initial State Exactly as Before',init
           write(6,1001)
           endif 


           call initl(init,nrank,wv1(1))
           anor=dsqrt(anor)

c           do 105 m=nrank*nnt+1,(nrank+1)*nnt
           do 105 m=1,nnt
              wv1(m)=wv1(m)/anor
105        continue

c        do i=nrank*nnt+1,(nrank+1)*nnt  
        do i=1,nnt  
         if( i+nrank*nnt == 3450) then
            wv1(i) = 1.0d0
         else
            wv1(i) = 0.0d0
         endif 
       enddo



        call lancz(nlanc,jp,nrank,wv1(1),wv2(1),wz(1)) 


        end if
9902    continue

        if(ilancz .eq. 2) then
           if(nrank == 0) then
           write(6,*) 'Write Output From Lancz'
           open(unit=12,file=fileb,form='unformatted',status='unknown')
           write(12) ndim,ndimu
           write(12) (wz(m),m=1,ndim)
           write(12) (mcodeu(m),m=1,ndimu)
           write(12) (mcoded(m),m=0,ndim/ndimu-1)
           close (unit=12)
           endif 
        end if

        if(nrank == 0) then
        call second(tt2)
        write(6,*) 'Lancz. CPU = ',tt2-tt1
        endif
        goto 999

C                                               Using Modified Lanczos
c299     continue
c
c        write(6,*) 'Modified Lanczos Method'
c        write(6,1001)
c        call lancm(nlanc,jp)

999     continue

        ndiag=mod(ndiag,10)
c    
c      
        if(ndiag .eq. 2 .and. ndim .le. iz) then
           write(6,*) 'G.S Eigenvector'
           write(6,1001)
           write(6,1008) (wz(m),m=1,ndim)
           write(6,1001)
        end if
1001    format(1H )
1008    format(2x,4(f12.7))
c
c
        if(ngs.eq.0) then
           if(nrank .eq. 0) then
           write(6,*) 'No Ground State Calculations'
           endif
           goto 10000
        end if

        if(nrank .eq. 0) then
        call second(tt1)
        write(6,*) 'Ground State Properties'
        write(6,1001)
        endif


        do 1950 l=1,icor
        do 1950 i=1,nsites
        do 1950 j=1,nsites
           corv(i,j,l)=zero
1950    continue

        if(iszsz .eq. 1) then
           if(nrank .eq. 0)then
              call second(tt5)
           endif 
           
c           call szsz(nrank,wv2(nrank*nnt+1),wz(nrank*nnt+1))
           call szsz(nrank,wv2(1),wz(1))

           if(nrank .eq. 0)then
              call second(tt6)
              write(6,*) '<SzSz>. CPU = ',tt6-tt5
           endif 
        end if

        if(isxsx .eq. 1) then
           if(nrank .eq. 0)then
              call second(tt5)
           endif 
           
c          call sxsx(nrank,wv1(nrank*nnt+1),wv2(nrank*nnt+1),
c     &                    wz(nrank*nnt+1))

          call sxsx(nrank,wv1(1),wv2(1),wz(1))
           if(nrank .eq. 0)then
              call second(tt6)
              write(6,*) '<SxSx>. CPU = ',tt6-tt5
           endif
        end if

c********************************************************************
c    the parameter ibxy and ijxjx eq 0 , otherwise we need to parallel
c    the two subroutine program bxybxy() and jxjx() 

        if(ibxy .ne. 0) then
           if(nrank .eq. 0) then
              call second(tt5)
           endif 
              call bxybxy
           if(nrank .eq. 0)then
              call second(tt6)
              write(6,*) '<B(i,j)>. CPU = ',tt6-tt5
           endif 
        end if

        if(ijxjx .eq. 1) then
           if(nrank .eq. 0)then
              call second(tt5)
           endif 
              call jxjx
           if(nrank .eq. 0) then
              call second(tt6)
              write(6,*) '<J(i,j)>. CPU = ',tt6-tt5
           endif 
        end if
c********************************************************************
c the parameter ipxpx eq 0 ,otherwise need to parallel these subroutine
c program 

        if(ipxpx .ne. 0) then
           if(nrank .eq. 0)  call second(tt5)

           if(ipxpx .eq. 1) call pxpx
           if(ipxpx .eq. 2) call pijkl
           if(ipxpx .eq. 3) call pijkl_r
           if(ipxpx .eq. 4) call pxpx_h

           if(nrank .eq. 0)  call second(tt6)
           if(nrank .eq. 0)  write(6,*) '<Pair(i,j)>. CPU = ',tt6-tt5
        end if

        do 1410 l=1,icor
        do 1410 i=2,nsites
        do 1410 j=1,i-1
           corv(j,i,l)=corv(i,j,l)
1410    continue

        do 1420 i=1,nsites
        do 1420 j=1,nsites
           scorz(i,j)=corv(i,j,1)+corv(i,j,2)-corv(i,j,3)
           scorx(i,j)=two*scorx(i,j)
1420    continue

        do 1440 i=1,nsites
           scorx(i,i)=scorz(i,i)
           corv(i,i,5)=corv(i,i,1)
           corv(i,i,6)=corv(i,i,2)
1440    continue

c**************************************************************
c       ibxy .eq. 0 

        if(ibxy .eq. 2) then
C                                       Redo Bond-Order
           do 1430 i=1,nsites
           do 1430 j=1,nsites
              ipx=ipxv(i)
              jpx=ipxv(j)
              tmpi1=corv(i,ipx,5)+corv(ipx,i,5)
              tmpi2=corv(i,ipx,6)+corv(ipx,i,6)
              tmpj1=corv(j,jpx,5)+corv(jpx,j,5)
              tmpj2=corv(j,jpx,6)+corv(jpx,j,6)
              bowv(i,j,1)=bowv(i,j,1)-(tmpi1+tmpi2)*(tmpj1+tmpj2)
c              bowv(i,j,1)=4.0*bowv(i,j,1)-(tmpi1+tmpi2)*(tmpj1+tmpj2)
              ipy=ipyv(i)
              jpy=ipyv(j)
              tmpi1=corv(i,ipy,5)+corv(ipy,i,5)
              tmpi2=corv(i,ipy,6)+corv(ipy,i,6)
              tmpj1=corv(j,jpy,5)+corv(jpy,j,5)
              tmpj2=corv(j,jpy,6)+corv(jpy,j,6)
              bowv(i,j,2)=bowv(i,j,2)-(tmpi1+tmpi2)*(tmpj1+tmpj2)
c              bowv(i,j,2)=4.0*bowv(i,j,2)-(tmpi1+tmpi2)*(tmpj1+tmpj2)
1430       continue
        end if

C                                               Fourier Transfer
        call frt
        
           ssz=zero
           ssx=zero
           ssnz=zero
           ssnx=zero
           do 1460 i=1,nsites
           do 1460 j=1,nsites
              ssz=ssz+scorz(i,j)
              ssx=ssx+scorx(i,j)
              ssnz=ssnz+nsub(i)*nsub(j)*scorz(i,j)
              ssnx=ssnx+nsub(i)*nsub(j)*scorx(i,j)
1460       continue
           ss=(ssx+ssx+ssz)/four
           ansite=dfloat(nsites)
           ssz=ssz/ansite
           ssx=ssx/ansite
           ssnz=ssnz/ansite
           ssnx=ssnx/ansite
           ssnz1=ssnz/ansite/4
           ssnx1=ssnx/ansite/4

           tenergy=zero
           do 1472 jb=1,numb
              i=jav(jb)
              j=jbv(jb)
              tenergy=tenergy+hopt(jb)*( corv(i,j,5)+corv(i,j,6) )
1472       continue
           tenergy=tenergy+tenergy
           penergy=zero
           do 1474 i=1,nsites
              penergy=penergy+uint(i)*corv(i,i,3)/2.0E0
     &               +epsi(i,1)*( corv(i,i,1)+corv(i,i,2) )
     &               +epsi(i,2)*( corv(i,i,1)-corv(i,i,2) )
1474       continue
           do 1476 ibond=1,numb
              ib=jav(ibond)
              jb=jbv(ibond)
              cdw=corv(ib,jb,1)+corv(ib,jb,2)+corv(ib,jb,3)
              sdw=corv(ib,jb,1)+corv(ib,jb,2)-corv(ib,jb,3)
              penergy=penergy+vint(ibond)*cdw
     &                       +ajzv(ibond)*sdw/four
1476       continue

           if(nrank == 0) then

           write(6,*) '<N(i)>'
           write(6,1122)(corv(i,i,1)+corv(i,i,2),i=1,nsites)
           write(6,*) '<N(i,+)>'
           write(6,1122) (corv(i,i,1),i=1,nsites)
           write(6,*) '<N(i,-)>'
           write(6,1122) (corv(i,i,2),i=1,nsites)
           write(6,*) '<2N(i,+)*N(i,-)>'
           write(6,1122) (corv(i,i,3),i=1,nsites)
1122       format(6x,8f8.5)
           write(6,1001)
           write(6,*) '<N(i,+)N(j,+)>'
           do 1461 i=1,nsites
              write(6,1111) (corv(i,j,1),j=1,nsites)
1461       continue
	   write(6,1111) (dreal(fk(j,1)),j=1,nsites)
           write(6,1111) (dimag(fk(j,1)),j=1,nsites)
           write(6,*) '<N(i,-)N(j,-)>'
           do 1462 i=1,nsites
              write(6,1111) (corv(i,j,2),j=1,nsites)
1462       continue
	   write(6,1111) (dreal(fk(j,2)),j=1,nsites)
           write(6,1111) (dimag(fk(j,2)),j=1,nsites)
           write(6,*) '<2N(i,+)N(j,-)>'
           do 1463 i=1,nsites
              write(6,1111) (corv(i,j,3),j=1,nsites)
1463       continue
	   write(6,1111) (dreal(fk(j,3)),j=1,nsites)
           write(6,1111) (dimag(fk(j,3)),j=1,nsites)
           write(6,*) '<CDW(q)>'
           write(6,1111) (dreal( fk(j,1)+fk(j,2)+fk(j,3) ),j=1,nsites)
           write(6,1111) (dimag( fk(j,1)+fk(j,2)+fk(j,3) ),j=1,nsites)
           write(6,*) '<Hole(i)Hole(j)>'
           do 1464 i=1,nsites
              write(6,1111) (corv(i,j,4),j=1,nsites)
1464       continue
	   write(6,1111) (dreal(fk(j,4)),j=1,nsites)
           write(6,1111) (dimag(fk(j,4)),j=1,nsites)
           write(6,1001)

           write(6,*) '<T>       = ',tenergy,tenergy/dfloat(nsites)
           write(6,*) '<P>       = ',penergy,penergy/dfloat(nsites)

           endif 

           tenergy=d(iorx)-penergy

           if(nrank == 0) then

           write(6,*) '<T>       = ',tenergy,tenergy/dfloat(nsites)
           write(6,*) '<D(i,+)C(j,+)>'
           do 1470 i=1,nsites
              write(6,1111) (corv(i,j,5),j=1,nsites)
1470       continue
	   write(6,1111) (dreal(fk(j,5)),j=1,nsites)
           write(6,1111) (dimag(fk(j,5)),j=1,nsites)
           write(6,*) '<D(i,-)C(j,-)>'
           do 1471 i=1,nsites
              write(6,1111) (corv(i,j,6),j=1,nsites)
1471       continue
	   write(6,1111) (dreal(fk(j,6)),j=1,nsites)
           write(6,1111) (dimag(fk(j,6)),j=1,nsites)
           write(6,1001)

           write(6,*) 'Bond Order'
           write(6,*) '<B(i,j,X)>'
           do 1475 i=1,nsites
              write(6,1111) (bowv(i,j,1),j=1,nsites)
1475       continue
	   write(6,1111) (dreal(fk(j,9)),j=1,nsites)
           write(6,1111) (dimag(fk(j,9)),j=1,nsites)

           endif 

        if(nd12 .eq. 2) then
           if(nrank == 0) then
           
           write(6,*) '<B(i,j,Y)>'
           do 1473 i=1,nsites
              write(6,1111) (bowv(i,j,2),j=1,nsites)
1473       continue
           write(6,1111) (dreal(fk(j,10)),j=1,nsites)
           write(6,1111) (dimag(fk(j,10)),j=1,nsites)

           endif 
        end if

        if(nrank == 0) then
           write(6,1001)

           write(6,*) 'Current-Current'
           write(6,*) '<J(i,j,X)>'
           do 1477 i=1,nsites
              write(6,1111) (curv(i,j),j=1,nsites)
1477       continue
           write(6,1111) (dreal(fk(j,11)),j=1,nsites)
           write(6,1111) (dimag(fk(j,11)),j=1,nsites)
           write(6,1001)

        endif 

        if(ipxpx .eq. 0) then
           if(nrank == 0) then
              write(6,*) 'No Pairing Correlations'
           endif 
           goto 699
        end if

        if(ipxpx .eq. 2 .or. ipxpx .eq. 3) then
           if(nrank == 0) then
              write(6,*) 'Making P(i,j,k,l) matrix. Run Pairing Program'
           endif 
           goto 699
        end if
          
        if(ipxpx .eq. 1) then
           if(nrank == 0) then
              write(6,*) 'Pairing-<D(l) D^+(k)>'
           endif 
        endif 
        
        if(ipxpx .eq. 4)  then
            if(nrank == 0) then
               write(6,*) 'Pairing-<D(l)^+ D(k)>'
            endif
        endif 

        if(nrank == 0) then
          write(6,*) 'Local Singlet =',pair(1,1)
c	   write(6,*) 'N. N. Singlet =',pair(2,2)-pair(2,nsites)
c	   write(6,*) 'N. N. Triplet =',pair(2,2)+pair(2,nsites)
        endif 

           ipx=iposit( mod( 1+lxs,lxs), 0 )
           imx=iposit( mod(-1+lxs,lxs), 0 )
           pxwave=pair(ipx,ipx)-pair(ipx,imx)
     &           -pair(imx,ipx)+pair(imx,imx)
          triplet=pair(ipx,ipx)+pair(ipx,imx)
     &           +pair(imx,ipx)+pair(imx,imx)
     
        if(nd12 .eq. 1) then
           if(nrank == 0) then
              write(6,*) 'N. N. Singlet = ',pxwave/two
              write(6,*) 'N. N. Triplet = ',triplet/two
           endif 
        end if

        if(nd12 .eq. 2) then
              ipy=iposit( 0, mod( 1+lys,lys) )
              imy=iposit( 0, mod(-1+lys,lys) )
          pywave=pair(ipy,ipy)-pair(ipy,imy)-pair(imy,ipy)+pair(imy,imy)
           swave=pair(ipx,ipx)+pair(ipx,imx)+pair(ipx,ipy)+pair(ipx,imy)
     &          +pair(imx,ipx)+pair(imx,imx)+pair(imx,ipy)+pair(imx,imy)
     &          +pair(ipy,ipx)+pair(ipy,imx)+pair(ipy,ipy)+pair(ipy,imy)
     &          +pair(imy,ipx)+pair(imy,imx)+pair(imy,ipy)+pair(imy,imy)
           dwave=pair(ipx,ipx)+pair(ipx,imx)-pair(ipx,ipy)-pair(ipx,imy)
     &          +pair(imx,ipx)+pair(imx,imx)-pair(imx,ipy)-pair(imx,imy)
     &          -pair(ipy,ipx)-pair(ipy,imx)+pair(ipy,ipy)+pair(ipy,imy)
     &          -pair(imy,ipx)-pair(imy,imx)+pair(imy,ipy)+pair(imy,imy)
                 write(6,*) 'P_x Pairing = ',pxwave/two
                 write(6,*) 'P_y Pairing = ',pywave/two
                 write(6,*) 'S-W Pairing = ',swave/four
                 write(6,*) 'D-W Pairing = ',dwave/four
        end if

        if(nrank == 0)then
           do 1478 i=1,nsites
              write(6,1111) (pair(i,j),j=1,nsites)
1478       continue
           write(6,1111) (dreal(fk(j,12)),j=1,nsites)
           write(6,1111) (dimag(fk(j,12)),j=1,nsites)
           write(6,*) 'Eigenvalues and Eigenvector of Pairing Function'
        endif 

           call tred2(lxy,nsites,pair,paird,pairwz,pairz)
           call imtql2(lxy,nsites,paird,pairwz,pairz,ier)
           if(ier .ne. 0) then
               if(nrank == 0) then
               write(6,*) 'ier = ',ier
               endif 
           endif 

        if(nrank == 0) then
           write(6,1117) (paird(i),i=1,nsites)

1117       format(2x,4(f12.7))
           do i=1,nsites
              write(6,1111) (pairz(i,j),j=1,nsites)
           end do
        endif 
699     continue
        if(nrank == 0) then
        write(6,1001)

           write(6,*) 'Spin-Spin Correlations'
           do 1480 i=1,nsites
              write(6,1111) (scorz(i,j),j=1,nsites)
1480       continue
           write(6,1111) (dreal(fk(j,7)),j=1,nsites)
           write(6,1111) (dimag(fk(j,7)),j=1,nsites)
           write(6,1113) ssz,ssnz,ssnz1
           do 1484 i=1,nsites
              write(6,1111) (scorx(i,j),j=1,nsites)
1484       continue
           write(6,1111) (dreal(fk(j,8)),j=1,nsites)
           write(6,1111) (dimag(fk(j,8)),j=1,nsites)
           write(6,1113) ssx,ssnx,ssnx1
           write(6,*) anor,' = 1 ?',' <S*S> = ',ss
1111       format(8f9.6)
c1111       format(1x,6f11.6)
1113       format(4x,'<M**2> =',f16.8,2x,'<N**2> =',2f16.8)
       endif

        if(idata .eq. 1) then
           ns8=1
           if(init .gt. 4) ns8=0
           if(nrank == 0)then
              write(8,*) ssz,ssnz,ssnz1
              write(8,*) ssx,ssnx,ssnx1
              write(8,*) ss
              write(8,*) ns8
           endif 
           do 8800 l=1,icor
              if(nrank == 0) then
                 write(8,*) l,coraw(l)
                 do 8810 i=1,nsites
                    write(8,*) i,(corv(i,j,l),j=1,nsites)
8810             continue
                 write(8,*) (fk(j,l),j=1,nsites)
              endif
8800       continue
        end if
        
        if(nrank == 0 )then
        call second(tt2)
        write(6,*) 'G.S. CPU = ',tt2-tt1
        endif 

C for the purpose of plotting
c      if(0) then
        if(nrank .eq. 0) then
           open (unit=4,file='plota',status='unknown')
           write(4,*) uint(1),vint(1),corv(1,1,3)/2.0E0,
     &                d(iorx)/float(nsites),
     &                tenergy/float(nsites),
     &                penergy/float(nsites)
           close (unit=4)
           open (unit=2,file='plotb',status='unknown')
           write(2,2008) nsites,nup+ndown,uint(1),vint(1),hopt(1)
2008    format('#lt N=',i2,' Ne=',i2,' U, V, t =',3f6.2)
           write(2,*) '<n(+)n(-)> = ',corv(1,1,3)/2.0E0
           write(2,*) '<H> = ',d(iorx),d(iorx)/dfloat(nsites)
           write(2,*) '<T> = ',tenergy,tenergy/dfloat(nsites)
           write(2,*) '<P> = ',penergy,penergy/dfloat(nsites)
           write(2,*) 'CDW(q) '
           do j=1,nsites
              write(2,2006) j,fk(j,1)+fk(j,2)+fk(j,3)
           end do
           write(2,*) 'SDW(q) '
           do j=1,nsites
              write(2,2006) j,fk(j,7)
           end do
           write(2,*) 'BOW(q) '
           do j=1,nsites
              write(2,2006) j,fk(j,9)
           end do
           write(2,*) 'CUR(q) '
           do j=1,nsites
              write(2,2006) j,fk(j,11)
           end do
           write(2,*) 'PxPx'
           write(2,*) pair(1,1),' Local Singlet'
           write(2,*) pair(2,2)-pair(2,nsites),' N. N. Singlet'
           write(2,*) pair(2,2)+pair(2,nsites),' N. N. Triplet'
2006    format(1x,i2,2x,2f10.6)
        close (unit=2)

        endif 
c      endif  

c***************************************************************
c        if(0) then
c***************************************************************
        if(iweig .eq. 1) then
           if(nrank .eq. 0)  call second(tt1)

        call gsweig(init,nrank,wv1(1),wv2(1),wz(1))

           if(nrank .eq. 0)  call second(tt2)
           if(nrank .eq. 0)  write(6,*) 'Weight. CPU = ',tt2-tt1
        end if

c***************************************************************
c        endif 
c***************************************************************

c       if(ijxjx .eq. 1) then
c       call second(tt1)
c       call jxjx
c       call second(tt2)
c       write(6,*) 'Jx|G>. CPU = ',tt2-tt1
c       end if

10000   continue

        if(nrank == 0) then
        call second(trune)
        write(6,*) 'Total CPU = ',trune-truns
        endif 


        call MPI_FINALIZE(ierr)
        stop
        end


C Decoding
C Note: we have to change mv(12,2) to mv(nsite,2) and compile
C       for different nsite

        subroutine decode(mv,nsite,ispin,ncode)
        include 'uvj.h'
        dimension mv(nsite,2)

        do 10 j=1,nsite
           mv(j,ispin)=mod(ncode,2)
           ncode=ncode/2
10      continue

        return
        end


C Coding

        subroutine code(mv,nsite,ispin,ncode)
c        include 'uvj.h'
        integer*8 mv(nsite,2),ncode,nbase,j
        integer*8 nsite,ispin

        ncode=0
        nbase=1
        do 10 j=1,nsite
           ncode=ncode+mv(j,ispin)*nbase
           nbase=2*nbase
10      continue

        return
        end


C Set Indices

        subroutine indexs(nid)
        include 'uvj.h'
        integer nid

C                                            Read in Bonding Indices

         if(nid == 0) then 
c             open (unit=3,file='inbond_08a',status='old')
c             open (unit=3,file='inbond_08',status='old')
             open (unit=3,file='inbond_36',status='old')
c             open (unit=3,file='inbond_32',status='old')
c             open (unit=3,file='inbond_24',status='old')
c             open (unit=3,file='inbond_16',status='old')
c            open (unit=3,file='inbond',status='old')
             read(3,3001) lattice_id
3001         format(a6)
             read(3,*) insite
             if(insite.ne.nsites) then
             write(6,*) 'Read in Wrong File'
               call exit
             end if
             read(3,*) ncor
             read(3,3002) (jrv(i),i=1,ncor)
             read(3,3002) (nwlv(i),i=1,ncor)
             read(3,3004) (nsub(j),j=1,nsites)
             
             read(3,*) a_1x,a_1y               ! For 1 & 2D only
             read(3,*) a_2x,a_2y
        endif 

        call MPI_BCAST(lattice,6,MPI_CHARACTER,0,MPI_COMM_WORLD,
     &                  ierr)
        call MPI_BCAST(insite,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ncor,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(jrv,ncor,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(nwlv,ncor,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(nsub,nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,
     &                    ierr)
        call MPI_BCAST(a_1x,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(a_1y,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(a_2x,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(a_2y,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)

        if( nid == 0) then 
            do j=1,nsites
               read(3,*) ixv(j),iyv(j),ipxv(j),ipyv(j)
            enddo
        endif

        call MPI_BCAST(ixv, nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(iyv, nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ipxv,nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ipyv,nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        
        do j=1,nsites
           iposit( ixv(j), iyv(j) )=j
           xv(j)=float(ixv(j))*a_1x+float(iyv(j))*a_2x
           yv(j)=float(ixv(j))*a_1y+float(iyv(j))*a_2y
        end do

        if(nid == 0) then 
            read(3,*) ak_1x,ak_1y,L_x
            read(3,*) ak_2x,ak_2y,L_y
        endif

        call MPI_BCAST(a_1x,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(a_1y,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(a_2x,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(a_2y,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(L_x, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(L_y, 1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        
        pi=dacos(-one)
        ak_1x=ak_1x*two*pi/dfloat(L_x)
        ak_1y=ak_1y*two*pi/dfloat(L_y)
        ak_2x=ak_2x*two*pi/dfloat(L_x)
        ak_2y=ak_2y*two*pi/dfloat(L_y)

        if(nid == 0) then         
            do j=1,nsites
               read(3,*) kxv(j),kyv(j)
            enddo
        endif

        call MPI_BCAST(kxv, nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(kyv, nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        
        do j=1,nsites
           akxv(j)=float(kxv(j))*ak_1x+float(kyv(j))*ak_2x
           akyv(j)=float(kxv(j))*ak_1y+float(kyv(j))*ak_2y
        end do

3002    format(1x,3x,36i3)
3004    format(1x,36i2)

        if(nid == 0) then
            read(3,*) nsym
        endif

        call MPI_BCAST(nsym,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        
        if(nid == 0) then
            do 12 isym=1,nsym
               read(3,3004) (isymv(j,isym),j=1,nsites)
12          continue
        endif 
        
        call MPI_BCAST(isymv,nsym*nsites,MPI_INTEGER8,0,
     &                 MPI_COMM_WORLD,ierr)
       
        if(nid == 0) then 
            read(3,*) nsymb
            do 14 isym=1,nsymb
               read(3,3004) (isymbv(j,isym),j=1,nsites/2)
14          continue
        endif

        call MPI_BCAST(nsymb,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(isymbv,nsymb*(nsites/2),MPI_INTEGER8,0,
     &                 MPI_COMM_WORLD,ierr)


        if(nid == 0) then 
            open (unit=14,file='uvinput',status='old')
            read(14,*) uinput,iuread
            read(14,*) vinput,ivread
            close (unit=14)
        endif

        call MPI_BCAST(uinput,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(vinput,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(iuread,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ivread,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)


        if(nid ==0) then 
           do 15 j=1,nsites
               read(3,*) uint(j),epsi(j,1),epsi(j,2)
               if(iuread .eq. 1) uint(j)=uinput
                   write(6,1004) j,uint(j),epsi(j,1),epsi(j,2)
15         continue
           write(6,1001)
           do 10 jb=1,numb
              read(3,*) jbx,jav(jb),jbv(jb),
     1               ajxv(jb),ajzv(jb),hopt(jb),vint(jb)
              if(jbx.ne.jb) then
                   
                 write(6,*) 'Wrong Bonds',jbx,jb
                 call exit
              end if
              if(ivread .eq. 1) vint(jb)=vinput

              write(6,1002) jb,jav(jb),jbv(jb),
     2                   ajxv(jb),ajzv(jb),hopt(jb),vint(jb)
10         continue
        endif
        
c        write(6,*) "read unit 3 is over"

        call MPI_BCAST(uint,nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(epsi(1,1),nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(epsi(1,2),nsites,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(jbx,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(jav,numb,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(jbv,numb,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ajxv,numb,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ajzv,numb,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(hopt,numb,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(vint,numb,MPI_REAL8,0,MPI_COMM_WORLD,ierr)


1001    format(1H )
1002    format(1x,'Bond__',i2,2x,i2,'==',i2,4x,
     3  'J =',2f6.3,3x,'T, V =',2f7.4)
1004    format(1x,'Site__',i2,2x,'U, E =',3f11.6)
        
        if(nid == 0) then
        write(6,1001)
        endif 

        if(nid ==0) then
            close (unit=3)
        endif

        iden2=2**nsites
        iden1=iden2-1

        if(nid == 0) then
            write(6,*) 'iden2,iden1 = ',iden2,iden1
        endif 
        
            if(nid .eq. 0) then
            write(6,*)"iden1,iden2"
            endif 
        do 20 j=1,nsites
            idenv(j)=2**(j-1)
20      continue

C                                               Ipxv, Ipyv
        do 50 ia=0,1
        do 50 ib=0,1
           zhopv(ia,ib)=0.0E0
           do 70 ic=0,1
           do 70 id=0,1
              zjv(ic,id,ia,ib)=0.0E0
70         continue
50      continue
C                                               Sign
        do 40 i=1,nsites
        do 40 j=1,nsites
           nsign1(i,j)=0
40      continue
        do 60 i=2,nsites
        do 60 j=1,i-1
           nsign1(i,j)=2**(i-1)-2**j
60      continue
        do 80 i=1,nsites-1
        do 80 j=i+1,nsites
           nsign1(i,j)=nsign1(j,i)
80      continue

        return
        end


C Set Hilbert Space.    Not For Inf. U

        subroutine nstate(nid)
        include 'uvj.h'
        integer nid


C                                           Position Vector IAV (UP)
           nc=nsites-nup
           ncongu = ndimui

c           call confgu(nup,nsites,ncongu)
           call confgu_case(nup,nsites,ncongu)
           ndimu=ncongu
C                                           Base Vector IBV (Down)
           nc=nsites-ndown
           ncongd = ndimdi
c           call confgd(ndown,nsites,ncongd)
           call confgd_case(ndown,nsites,ncongd)

        if(ncongu*ncongd .ne. ndim) then
           write(6,*) 'ncongu * ncongd != ndim',ncongu,ncongd,ndim
           stop
        end if

        return
        end


        subroutine confgu_case(nx,nl,ncong)
        include 'uvj.h'
        integer*8 n1,n2,n3,n4,n5,n6,n7,n8
        integer*8 index,nx
        

        select case(nx)

           case(1)
             index = 1
             do n1=1,nl
                        mcodeu(index) = 2**(n1-1)
                        index = index+1
             enddo 
           
           case(2)
             index = 1
             do n1=2,nl
               do n2=1,n1-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)
                        index = index+1
               enddo
             enddo 
                
           case(3)
             index = 1
             do n1=3,nl
               do n2=2,n1-1
                  do n3=1,n2-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)
                        index = index+1
                  enddo
               enddo
             enddo 

           case(4)
             index = 1
             do n1=4,nl
               do n2=3,n1-1
                  do n3=2,n2-1
                     do n4=1,n3-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)
                        index = index+1
                     enddo
                  enddo
               enddo
             enddo 

           case(5)
             index = 1
             do n1=5,nl
               do n2=4,n1-1
                  do n3=3,n2-1
                     do n4=2,n3-1
                     do n5=1,n4-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)
                        index = index+1
                     enddo
                     enddo
                  enddo
               enddo
             enddo 

           case(6)
             index = 1
             do n1=6,nl
               do n2=5,n1-1
                  do n3=4,n2-1
                     do n4=3,n3-1
                     do n5=2,n4-1
                     do n6=1,n5-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)+2**(n6-1)
                        index = index+1
                     enddo
                     enddo
                     enddo
                  enddo
               enddo
             enddo 

           case(7)
             index = 1
             do n1=7,nl
               do n2=6,n1-1
                  do n3=5,n2-1
                     do n4=4,n3-1
                     do n5=3,n4-1
                     do n6=2,n5-1
                     do n7=1,n6-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)+2**(n6-1)+
     &                                  2**(n7-1)
                        index = index+1
                     enddo
                     enddo
                     enddo
                     enddo
                  enddo
               enddo
             enddo 
           case(8)
             index = 1
             do n1=8,nl
               do n2=7,n1-1
                  do n3=6,n2-1
                     do n4=5,n3-1
                     do n5=4,n4-1
                     do n6=3,n5-1
                     do n7=2,n6-1
                     do n8=1,n7-1
                        mcodeu(index) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)+2**(n6-1)+
     &                                  2**(n7-1)+2**(n8-1)
                        index = index+1
                     enddo
                     enddo
                     enddo
                     enddo
                  enddo
               enddo
             enddo 
            enddo
        end select


        if(nx .gt. 8) then
                call confgu(nx,nl,ncong)
        endif 

        return 
        end 

        subroutine confgd_case(nx,nl,ncong)
        include 'uvj.h'
        integer*8 n1,n2,n3,n4,n5,n6,n7,n8
        integer*8 index
        

        select case(nx)
           case(1)
             index = 1
             do n1=1,nl
                      mcoded(index-1) = 2**(n1-1)
                      index = index+1
             enddo 
           
           case(2)
             index = 1
             do n1=2,nl
               do n2=1,n1-1
                     mcoded(index-1) = 2**(n1-1)+2**(n2-1)
                     index = index+1
               enddo
             enddo 
                
           case(3)
             index = 1
             do n1=3,nl
               do n2=2,n1-1
                  do n3=1,n2-1
                     mcoded(index-1) = 2**(n1-1)+2**(n2-1)+
     &                                 2**(n3-1)
                        index = index+1
                  enddo
               enddo
             enddo 

           case(4)
             index = 1
             do n1=4,nl
               do n2=3,n1-1
                  do n3=2,n2-1
                     do n4=1,n3-1
                      mcoded(index-1) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)
                        index = index+1
                     enddo
                  enddo
               enddo
             enddo 

           case(5)
             index = 1
             do n1=5,nl
               do n2=4,n1-1
                  do n3=3,n2-1
                     do n4=2,n3-1
                     do n5=1,n4-1
                      mcoded(index-1) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)
                        index = index+1
                     enddo
                     enddo
                  enddo
               enddo
             enddo 

           case(6)
             index = 1
             do n1=6,nl
               do n2=5,n1-1
                  do n3=4,n2-1
                     do n4=3,n3-1
                     do n5=2,n4-1
                     do n6=1,n5-1
                      mcoded(index-1) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)+2**(n6-1)
                        index = index+1
                     enddo
                     enddo
                     enddo
                  enddo
               enddo
             enddo 

           case(7)
             index = 1
             do n1=7,nl
               do n2=6,n1-1
                  do n3=5,n2-1
                     do n4=4,n3-1
                     do n5=3,n4-1
                     do n6=2,n5-1
                     do n7=1,n6-1
                      mcoded(index-1) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)+2**(n6-1)+
     &                                  2**(n7-1)
                        index = index+1
                     enddo
                     enddo
                     enddo
                     enddo
                  enddo
               enddo
             enddo 
           case(8)
             index = 1
             do n1=8,nl
               do n2=7,n1-1
                  do n3=6,n2-1
                     do n4=5,n3-1
                     do n5=4,n4-1
                     do n6=3,n5-1
                     do n7=2,n6-1
                     do n8=1,n7-1
                      mcoded(index-1) = 2**(n1-1)+2**(n2-1)+
     &                                  2**(n3-1)+2**(n4-1)+
     &                                  2**(n5-1)+2**(n6-1)+
     &                                  2**(n7-1)+2**(n8-1)
                        index = index+1
                     enddo
                     enddo
                     enddo
                     enddo
                     enddo
                  enddo
               enddo
             enddo 
        end select

        if(nx .gt. 8) then
               call  confgd(nx,nl,ncong)
        endif 

        return 
        end 
C      Distributing NX Particles on Lattice of Site NL

        subroutine confgu(nx,nl,ncong)
        include 'uvj.h'

C       the nlower and nupper is the min and max+1 of the nx up, 
C       nl-nx down configuration 
       
c        nupper=1
        nupper=0
        numx=nl-1
        do 10 j=1,nx
           nupper=nupper+2**numx
           numx=numx-1
10      continue
        nlower=0
        numx=0
        do 20 j=1,nx
           nlower=nlower+2**numx
           numx=numx+1
20      continue
        k=0
        do 100 j=nlower,nupper
           ncode=j
           mu=0
           do 120 jx=1,nl
              mu=mu+mod(ncode,2)
              ncode=ncode/2
120        continue
           if(mu.ne.nx) goto 100
           k=k+1
            mcodeu(k) = j
c           ncvec(k)=j  
c  k correspond to the third column of the table I
100     continue
        if(k.ne.ncong) then
           write(6,*) 'Wrong # of Configuration k =',k,ncong
           stop
        end if

        return
        end


        subroutine confgd(nx,nl,ncong)
        include 'uvj.h'

C       the nlower and nupper is the min and max+1 of the nx up, 
C       nl-nx down configuration 
       
c        nupper=1
        nupper=0
        numx=nl-1
        do 10 j=1,nx
           nupper=nupper+2**numx
           numx=numx-1
10      continue
        nlower=0
        numx=0
        do 20 j=1,nx
           nlower=nlower+2**numx
           numx=numx+1
20      continue
        k=0
        do 100 j=nlower,nupper
           ncode=j
           mu=0
           do 120 jx=1,nl
              mu=mu+mod(ncode,2)
              ncode=ncode/2
120        continue
           if(mu.ne.nx) goto 100
           k=k+1
           mcoded(k-1) = j
c           ncvec(k)=j  
c  k correspond to the third column of the table I
100     continue
        if(k.ne.ncong) then
           write(6,*) 'Wrong # of Configuration k =',k,ncong
           stop
        end if

        return
        end



        integer*8 function nfac(nx,ny,nz)

c        if(nz.gt.16) then
c           write(6,*) 'Integer is Too Big to Calculate NFAC'
c           stop
c        end if
        ier=0
        if(nx.gt.ny) then
           na=ny
           nb=nx
        else
           na=nx
           nb=ny
        end if
        nfacx=1
C       nfacx = !na
        do 10 k=1,na
           nfacx=k*nfacx
10      continue
        nfacy=1
        do 20 k=nb+1,nz
           nfacy=k*nfacy
20      continue
        if(nfacx.lt.0 .or. nfacy.lt.0) ier=1
        nfacz=mod(nfacy,nfacx)
        if(nfacz.ne.0) ier=2
        if(ier.ne.0) write(6,*) 'Wrong Factorization. ier =',ier

C       nfac = C nz get nx   Cnz^nx
        nfac=nfacy/nfacx

        return
        end


C Construct H Matrix and Obtain Eigenvalues & Eigenvector
C                               Use IMTQL2

        subroutine hmatx(nid)
        include 'uvj.h'
        integer nid
        real*8 wv1(ndim),wv2(ndim),azdia(ndim)

        do 910 i=1,ndim
           wv1(i)=zero
910     continue

        n1=1
        n2=2
        do 900 i=1,ndim
           do 930 j=1,ndim
              wv2(j)=zero
930        continue
           wv1(i)=one
           call hop(n1,n2)
           do 950 j=1,ndim
              trih(j,i)=wv2(j)
950        continue
           wv1(i)=zero
900     continue

        if(ndim.le.16) then
           do 970 i=1,ndim
              write(6,1003) (trih(j,i),j=1,ndim)
              write(6,*) ' '
970        continue
        end if
        if(ndim .gt. 16 .and. ndim .le. 36) then
           do 975 i=1,ndim
           write(6,*) 'I = ',i
           do 975 j=1,ndim
              if(trih(i,j) .ne. 0.0) then
                 write(6,*) 'j, H ',j,trih(j,i)
              end if
           if(j .eq. ndim) write(6,*)  ' '
975        continue
        end if
1003    format(2x,8(f7.3,1x))

        if(ndiag .eq. 14 .or. ndiag .eq. 15) then
           if(nid == 0) then 
                open (unit=13,file='hm_t',form='unformatted',
     &                 status='unknown')
           end if
           write(13) ndim
           do i=1,ndim
           ncol=0
           do j=1,ndim
           if(trih(i,j) .ne. 0) then
               ncol=ncol+1
               wv1(ncol)=j
               wv2(ncol)=trih(i,j)
           end if
           end do
              write(13) i,ncol
              write(13) ( nint( wv1(j) ), j=1,ncol )
              write(13) ( wv2(j),       j=1,ncol )
           end do
           close (unit=13)
           write(6,*) 'Saved H_Matrix into disk'
           if(ndiag .eq. 14) stop 'H is not diagonalized'
           
           if(nid == 0) then
                open (unit=13,file='hm_u',form='unformatted',
     &                 status='unknown')
                read(13) ndim
                read(13) (azdia(m),m=1,ndim)
           endif 

           do m=1,ndim
                trih(m,m)=trih(m,m)+azdia(m)
           end do
           close (unit=13)

           if(nid == 0) then
               open (unit=13,file='hm_v',form='unformatted',
     &                   status='unknown')
               read(13) ndim
               read(13) (azdia(m),m=1,ndim)
           endif 

           do m=1,ndim
              trih(m,m)=trih(m,m)+azdia(m)
           end do
           if(nid == 0) then
               close (unit=13)
           end if

        end if

	write(6,*) 'ndim =',ndim
        do i=1,ndim
        do j=1,ndim
           if( trih(i,j) .ne. trih(j,i) ) then
              write(6,*) i,j,trih(i,j),trih(j,i)
           end if
        end do
        end do

        call tred2(iz,ndim,trih,d,wz,z)
        call imtql2(iz,ndim,d,wz,z,ier)
        if(ier.ne.0) write(6,*) 'ier = ',ier

        write(6,*) 'Eigenvalues of H-Matrix'
        write(6,1001)
        write(6,*) 'E0-E1, E2-E1 = ',d(2)-d(1),d(3)-d(2)
        write(6,1002) (d(i),i=1,ndim)
        write(6,1001)
1001    format(1H )
1002    format(2x,4(f12.7))

	if(ndim .le. 36) then
	   open (unit=2,file='wzall',status='unknown')
	   do m=1,ndim
	      write(2,*) 'm = ',m
	      write(2,*) (z(j,m),j=1,ndim)
	      write(2,*) ' '
	   end do
	end if

        return
        end


C H Operation.    Correlated Electron Models.

        subroutine hop(n1,n2,nid,wv1,wv2)
        include 'uvj.h'
        integer nid
        integer*8, external ::find_index

        type send_data
              real*8   tmp
              integer*8  nn_jx
        end type   

        type n_send_recv
             integer*8,allocatable::n_jx(:)
             integer*8,allocatable::n_i(:)
        end type

        integer*8 ncount
        integer*8,allocatable::ncount_nsize(:),nc_id(:)

        real*8 wv1(1:nnt)
        real*8 wv2(1:nnt)
        integer status(MPI_STATUS_SIZE),request
        integer*8 na_count,count_test
        LOGICAL flag
        type(send_data),allocatable::tmp_data(:),tmp_all(:)
        type(n_send_recv),allocatable::n_mesg(:)
        integer Block(2),Disp(2),Tp(2),newtype
        real*8 st_time,ed_time
        real*8,save::dur_time=0.0D0
        real*8 st_time_1,ed_time_1
        real*8,save::hop_time=0.0D0
        real*8 az_dia
        integer*8 nid_nnt
        integer*8 ibvn,iavn
c        integer*8 n_left,n_right,n_mid

c        write(6,*) "the first enter the subroutine hop",nid
        nid_nnt = nid*nnt

        Block(1)=1
        Disp(1) =0
        Tp(1)   =MPI_REAL8

        Block(2)=1
        Disp(2) =8
        Tp(2)   =MPI_INTEGER8

        call MPI_TYPE_STRUCT(2,Block,Disp,Tp,newtype,ierr)
        call MPI_TYPE_COMMIT(newtype,ierr)


        allocate(ncount_nsize(0:(nsize-1)),nc_id(0:(nsize-1)))
        allocate(n_mesg(0:(nsize-1)))

C                                               Off-Diagonal
        do 1001 jb=1,numb

c        write(6,*) "jb == , nid == ",jb , nid
           iaget=idenv(jav(jb))
           ibget=idenv(jbv(jb))
           icget=nsign1(jav(jb),jbv(jb))
           nfac=iaget-ibget
           hoptq=hopt(jb)
           zhopv(1,0)=hoptq
           zhopv(0,1)=hoptq
           ajxq=ajxv(jb)/2.0E0
           zjv(1,0,0,1)=ajxq
           zjv(0,1,1,0)=ajxq
           
           vq=vint(jb)
           ajzq=ajzv(jb)/four
c           ideni = idenv(jb)  == idenv(jav(jb))
           uq=uint(jb) 
           eq=epsi(jb,1)
           hq=epsi(jb,2)
      
        
        if(n1 .eq. 1) then
c********************************************************
           do i=0,nsize-1
                ncount_nsize(i)=0  
                nc_id(i)=0
           enddo
           
           az_dia = 0.0D0 
           ncount = 0

c************************************************************
        if(nnt .ge. ndimu) then
c************************************************************
        
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 140 indexd = indexd_s,indexd_t
              ncoded=mcoded(indexd)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget

                do indexu=1,ndimu
                   ncodeu=mcodeu(indexu)
                   iup=iand(iaget,ncodeu)/iaget 
                   jup=iand(ibget,ncodeu)/ibget

              az_dia =   vq*dfloat( (iup+idw)*(jup+jdw) ) 
     &                  + ajzq*dfloat( (iup-idw)*(jup-jdw) )
     &                  + uq*dfloat( iup*idw )
     &                  + eq*dfloat( iup+idw )
     &                  + hq*dfloat( iup-idw )

                i = indexd*ndimu + indexu
c              wv2(i)=wv2(i)+az_dia*wv1(i)
           wv2(i-nid_nnt)=wv2(i-nid_nnt)+az_dia*wv1(i-nid_nnt)
                enddo

              if(idw .eq. jdw) goto 140
              icoded=ncoded+(jdw-idw)*nfac

c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui

              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui



            do indexu=1,ndimu
              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt

              if(j_id .ne. nid ) then 
                ncount = ncount+1
                ncount_nsize(j_id) = ncount_nsize(j_id)+1
              endif 
                        
            enddo
  140    continue

c********************************************************
        else
c********************************************************
           do 100 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu

              ncoded=mcoded(indexd)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget

              ncodeu=mcodeu(indexu)
              iup=iand(iaget,ncodeu)/iaget 
              jup=iand(ibget,ncodeu)/ibget
              
              az_dia =   vq*dfloat( (iup+idw)*(jup+jdw) ) 
     &                  + ajzq*dfloat( (iup-idw)*(jup-jdw) )
     &                  + uq*dfloat( iup*idw )
     &                  + eq*dfloat( iup+idw )
     &                  + hq*dfloat( iup-idw )

              i_nnt = i - nid_nnt
              wv2(i_nnt)=wv2(i_nnt)+az_dia*wv1(i_nnt)
c              wv2(i)=wv2(i)+az_dia*wv1(i)



              if(idw .eq. jdw) goto 100
c
C             '10' or '01' change to '01' or '10'
              icoded=ncoded+(jdw-idw)*nfac
c              ibvn=ibv(icoded)
c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui
C
              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt

              if(j_id .ne. nid ) then 
                ncount = ncount+1
                ncount_nsize(j_id) = ncount_nsize(j_id)+1
              endif 
100        continue      

c*****************************************************************
        endif
c*****************************************************************

           if(ncount .ne. 0) then
             do i=0,nsize-1
                if(ncount_nsize(i) .ne. 0)then 
                        allocate( n_mesg(i)%n_jx(ncount_nsize(i)) )
                        allocate( n_mesg(i)%n_i(ncount_nsize(i)) )
                        nc_id(i) = 1
                endif 
             enddo
           endif

c***************************************************************
        if(nnt .ge. ndimu) then
c***************************************************************
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 141 indexd = indexd_s,indexd_t
              ncoded=mcoded(indexd)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget
              if(idw .eq. jdw) goto 141

              icoded=ncoded+(jdw-idw)*nfac

c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui
c              ibvn=ibv(icoded)

              if(icget .ne. 0) then
                 icd=iand(icget,ncoded)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icd,2)
                    icd= icd/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icd = 1.0
                 else
                    asignv_icd = -1.0
                 endif 
              else
                    asignv_icd = 1.0
              endif 

              do indexu = 1,ndimu

              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt
              i = indexd*ndimu + indexu

              if(j_id == nid) then
                 xd=zhopv(idw,jdw)*asignv_icd
                 wv2(i-nid_nnt)=wv2(i-nid_nnt)+xd*wv1(jx-nid_nnt)
              else
                   n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                   n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icd
                   nc_id(j_id) = nc_id(j_id)+1
              endif
                
              enddo                  
        
  141     continue 

c*****************************************************
         else
c*****************************************************
           do 101 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu

              ncoded=mcoded(indexd)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget
              if(idw .eq. jdw) goto 101
              icoded=ncoded+(jdw-idw)*nfac
c              ibvn=ibv(icoded)
c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui

              
              if(icget .ne. 0) then
                 icd=iand(icget,ncoded)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icd,2)
                    icd= icd/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icd = 1.0
                 else
                    asignv_icd = -1.0
                 endif 
              else
                    asignv_icd = 1.0
              endif 
C
              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt

              if(j_id == nid) then
                 xd=zhopv(idw,jdw)*asignv_icd
c                 wv2(i)=wv2(i)+xd*wv1(jx)
                 wv2(i-nid_nnt)=wv2(i-nid_nnt)+xd*wv1(jx-nid_nnt)
              else
                   n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                   n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icd
                   nc_id(j_id) = nc_id(j_id)+1
              endif
101       continue

c**************************************************************
        endif 
c*************************************************************

         if(ncount .ne. 0) then
             allocate(tmp_all(ncount))

          do 102 i=0,nsize-1
                count_test = 0
          if( nid .ne. i) then
            if(ncount_nsize(i) .ne. 0) then
                allocate(tmp_data(ncount_nsize(i)))
                do j=1,ncount_nsize(i)
c                   jj_icd = abs(n_mesg(i)%n_i(j))
                jj_icd = abs(n_mesg(i)%n_i(j))
                   tmp_data(j)%tmp = (n_mesg(i)%n_i(j))/jj_icd *
     &                                wv1(jj_icd-nid_nnt)
                   tmp_data(j)%nn_jx = n_mesg(i)%n_jx(j)
                enddo 
          call MPI_ISEND(tmp_data,ncount_nsize(i),newtype,i,
     &             i, MPI_COMM_WORLD,request,ierr)
                        
          call MPI_WAIT(request,status,ierr)
                deallocate(tmp_data)

            endif ! ncount_nsize(i) .ne. 0 

          else ! nid .ne. i
                
                 do 103 ,while (count_test .lt. ncount)
          call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,flag,status,ierr)

                if(flag == .true. ) then

                    j_id = status(MPI_SOURCE)
                    j_tag = status(MPI_TAG)
          call MPI_RECV(tmp_all,ncount_nsize(j_id),newtype,j_id,
     &           j_tag,MPI_COMM_WORLD,status,ierr)

                 j_size = ncount_nsize(j_id)
                do jj = 1,j_size
c                      wv2(tmp_all(jj)%nn_jx) = wv2(tmp_all(jj)%nn_jx)+
c     &                hoptq*tmp_all(jj)%tmp
         wv2(tmp_all(jj)%nn_jx-nid_nnt) = wv2(tmp_all(jj)%nn_jx-nid_nnt)
     &                      + hoptq*tmp_all(jj)%tmp
                enddo 
                count_test = count_test +j_size
               endif 
103      continue

             end if ! if(nid != i)
102      continue
         
         deallocate(tmp_all)
        end if
c***********************************************
c        this barrier can be not need 

c        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
           if(ncount .ne. 0) then
             do i=0,nsize-1
                if(ncount_nsize(i) .ne. 0)then 
                        deallocate( n_mesg(i)%n_jx )
                        deallocate( n_mesg(i)%n_i )
                endif 
             enddo
           endif

    

       do i=0,nsize-1
            ncount_nsize(i)=0  
            nc_id(i)=0
           enddo
        ncount = 0
c*******************************************************
        if(nnt .ge. ndimu) then
c*******************************************************
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 115 indexu = 1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 115
               
              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn = ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)
              
              do indexd = indexd_s,indexd_t
                 jx=iavn+indexd*ndimu

                 j_id = (jx-1)/nnt

                 if(j_id .ne. nid ) then
                    ncount = ncount+1
                    ncount_nsize(j_id) = ncount_nsize(j_id)+1
                 endif 
                 
              enddo
  115     continue 


c*******************************************************
        else
c*******************************************************
           do 105 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu

              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 105

              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn = ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)


c              icu=iand(icget,ncodeu)
c              xu=zhopv(iup,jup)*asignv(icu)
C
              jx=iavn+indexd*ndimu

              j_id = (jx-1)/nnt

              if(j_id .ne. nid ) then
                 ncount = ncount+1
                 ncount_nsize(j_id) = ncount_nsize(j_id)+1
              endif 

105        continue

c****************************************************************
        endif 
c****************************************************************

           if(ncount .ne. 0) then
              do i=0,nsize-1
                 if(ncount_nsize(i) .ne. 0)then 
                         allocate( n_mesg(i)%n_jx(ncount_nsize(i)) )
                         allocate( n_mesg(i)%n_i(ncount_nsize(i)) )
                         nc_id(i) = 1
                 endif 
              enddo
            endif


c*****************************************************************
        if(nnt .ge. ndimu) then
c*****************************************************************
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 116 indexu = 1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 116

              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn = ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)

              if(icget .ne. 0) then
                 icu=iand(icget,ncodeu)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icu,2)
                    icu= icu/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icu = 1.0
                 else
                    asignv_icu = -1.0
                 endif 
              else
                    asignv_icu = 1.0
              endif 


              do indexd = indexd_s,indexd_t
                 jx=iavn+indexd*ndimu

                 j_id = (jx-1)/nnt
                 i = indexd*ndimu+indexu

                 if(j_id == nid) then
                    xu=zhopv(iup,jup)*asignv_icu
c                    wv2(i)=wv2(i)+xu*wv1(jx)
                 wv2(i-nid_nnt)=wv2(i-nid_nnt)+xu*wv1(jx-nid_nnt)
                 else
                    n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                    n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icu
                    nc_id(j_id) = nc_id(j_id)+1
                 endif
              enddo
  116     continue
c*****************************************************************
        else
c*****************************************************************
           do 106 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu

              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 106

              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn = ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)
               

c              icu=iand(icget,ncodeu)
c              xu=zhopv(iup,jup)*asignv(icu)
C
              if(icget .ne. 0) then
                 icu=iand(icget,ncodeu)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icu,2)
                    icu= icu/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icu = 1.0
                 else
                    asignv_icu = -1.0
                 endif 
              else
                    asignv_icu = 1.0
              endif 


              jx=iavn+indexd*ndimu

              j_id = (jx-1)/nnt

              if(j_id == nid) then
                 xu=zhopv(iup,jup)*asignv_icu
c                 wv2(i)=wv2(i)+xu*wv1(jx)
                 wv2(i-nid_nnt)=wv2(i-nid_nnt)+xu*wv1(jx-nid_nnt)
              else
                    n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                    n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icu
                    nc_id(j_id) = nc_id(j_id)+1
              endif
106        continue
c*******************************************************************
        endif
c*******************************************************************


        if(ncount .ne. 0) then          

           allocate(tmp_all(ncount))
        count_test = 0
          do 107 i=0,nsize-1
              if( nid .ne. i) then
                if(ncount_nsize(i) .ne. 0) then
                 allocate(tmp_data(ncount_nsize(i)))
                 do j=1,ncount_nsize(i)
c                   jj_icd = abs(n_mesg(i)%n_i(j))
                   jj_icd = abs(n_mesg(i)%n_i(j))
                   tmp_data(j)%tmp = (n_mesg(i)%n_i(j))/jj_icd *
     &                                wv1(jj_icd-nid_nnt)
                    tmp_data(j)%nn_jx = n_mesg(i)%n_jx(j)
                 enddo
           call MPI_ISEND(tmp_data,ncount_nsize(i),newtype,i,
     &             i, MPI_COMM_WORLD,request,ierr)
 
           call MPI_WAIT(request,status,ierr)
                 deallocate(tmp_data)
 
                endif                 

              else
                 do 108 , while (count_test .lt. ncount)
          call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,flag,status,ierr)
                      
                  if(flag .eq. .true.  )then
                    j_id = status(MPI_SOURCE)
                    j_tag = status(MPI_TAG)

         call MPI_RECV(tmp_all,ncount_nsize(j_id),newtype,j_id,
     &           j_tag,MPI_COMM_WORLD,status,ierr)

                j_size = ncount_nsize(j_id)
                 do jj = 1,j_size
c                       wv2(tmp_all(jj)%nn_jx) = wv2(tmp_all(jj)%nn_jx)+
c     &                 hoptq*tmp_all(jj)%tmp
        wv2(tmp_all(jj)%nn_jx-nid_nnt) = wv2(tmp_all(jj)%nn_jx-nid_nnt)+
     &                 hoptq*tmp_all(jj)%tmp

                 enddo

                count_test = count_test + j_size
                endif 
108              continue

             endif 
107      continue
         
        deallocate(tmp_all)
        end if

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
           if(ncount .ne. 0) then
             do i=0,nsize-1
                if(ncount_nsize(i) .ne. 0)then 
                        deallocate( n_mesg(i)%n_jx )
                        deallocate( n_mesg(i)%n_i )
                endif 
             enddo
           endif
c*************************Spin up*****************************

        else if(n1 .eq. 2) then


          do i=0,nsize-1
                 ncount_nsize(i)=0
                 nc_id(i)=0
          enddo         
           ncount = 0


c**************************************************************
        if(nnt .ge. ndimu) then
c**************************************************************
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 210 indexd = indexd_s,indexd_t
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget

                do indexu=1,ndimu
                   ncodeu=mcodeu(indexu)
c                   ncodeu=ncvec(indexu)
                   iup=iand(iaget,ncodeu)/iaget 
                   jup=iand(ibget,ncodeu)/ibget

              az_dia =   vq*dfloat( (iup+idw)*(jup+jdw) ) 
     &                  + ajzq*dfloat( (iup-idw)*(jup-jdw) )
     &                  + uq*dfloat( iup*idw )
     &                  + eq*dfloat( iup+idw )
     &                  + hq*dfloat( iup-idw )

                i = indexd*ndimu + indexu
c              wv1(i)=wv1(i)+az_dia*wv2(i)
              wv1(i-nid_nnt)=wv1(i-nid_nnt)+az_dia*wv2(i-nid_nnt)
                enddo

              if(idw .eq. jdw) goto 210
              icoded=ncoded+(jdw-idw)*nfac
c              ibvn=ibv(icoded)
c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui

            do indexu=1,ndimu
              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt

              if(j_id .ne. nid ) then 
                ncount = ncount+1
                ncount_nsize(j_id) = ncount_nsize(j_id)+1
              endif 
                        
            enddo
  210    continue

c**************************************************************
        else
c**************************************************************
           do 200 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu

              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget
              
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget 
              jup=iand(ibget,ncodeu)/ibget
              
              az_dia =   vq*dfloat( (iup+idw)*(jup+jdw) ) 
     &                 + ajzq*dfloat( (iup-idw)*(jup-jdw) )
     &                  + uq*dfloat( iup*idw )
     &                  + eq*dfloat( iup+idw )
     &                  + hq*dfloat( iup-idw )

c              wv1(i)=wv1(i)+az_dia*wv2(i)
              wv1(i-nid_nnt)=wv1(i-nid_nnt)+az_dia*wv2(i-nid_nnt)
              if(idw .eq. jdw) goto 200
c
C             '10' or '01' change to '01' or '10'
              icoded=ncoded+(jdw-idw)*nfac
c              ibvn=ibv(icoded)
c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui
              
              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 
              j_id = (jx-1)/nnt

              if(j_id .ne. nid ) then 
                 ncount = ncount+1
                 ncount_nsize(j_id) = ncount_nsize(j_id)+1
              endif 

200        continue 
c***************************************************************
        endif 
c***************************************************************

           if(ncount .ne. 0) then
              do i=0,nsize-1
                 if(ncount_nsize(i) .ne. 0)then
                         allocate( n_mesg(i)%n_jx(ncount_nsize(i)) )
                         allocate( n_mesg(i)%n_i(ncount_nsize(i)) )
                         nc_id(i) = 1
                 endif
              enddo
            endif

c***************************************************************
        if(nnt .ge. ndimu) then
c**************************************************************

           indexd_s = (nid*nnt)/ndimu
           indexd_t = ((nid+1)*nnt-1)/ndimu

        do 241 indexd = indexd_s,indexd_t
                
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget
              if(idw .eq. jdw) goto 241

              icoded=ncoded+(jdw-idw)*nfac
c              ibvn=ibv(icoded)
c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui
              
              if(icget .ne. 0) then
                 icd=iand(icget,ncoded)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icd,2)
                    icd= icd/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icd = 1.0
                 else
                    asignv_icd = -1.0
                 endif 
              else 
                    asignv_icd = 1.0
              endif 

              do indexu = 1,ndimu
              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt
              i = indexd*ndimu+indexu

              if(j_id == nid) then
                 xd=zhopv(idw,jdw)*asignv_icd
c                 wv1(i)=wv1(i)+xd*wv2(jx)
                 wv1(i-nid_nnt)=wv1(i-nid_nnt)+xd*wv2(jx-nid_nnt)
              else
                   n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                   n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icd
                   nc_id(j_id) = nc_id(j_id)+1
              end if

              enddo

 241    continue 

c***********************************************************
       else 
c***********************************************************
           do 201 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu

              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iaget,ncoded)/iaget
              jdw=iand(ibget,ncoded)/ibget
              if(idw .eq. jdw) goto 201
c
C             '10' or '01' change to '01' or '10'
              icoded=ncoded+(jdw-idw)*nfac
c              ibvn=ibv(icoded)
c              do ib =1, ndimdi
c                  if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvn = (ib-1)*ndimui
              ibvn = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui

c              icd=iand(icget,ncoded)
c              xd=zhopv(idw,jdw)*asignv(icd)
C
              if(icget .ne. 0) then
                 icd=iand(icget,ncoded)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icd,2)
                    icd= icd/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icd = 1.0
                 else
                    asignv_icd = -1.0
                 endif 
              else 
                    asignv_icd = 1.0
              endif 

              jx=indexu+ibvn        ! jx corrspone to I' ,i  corrspone to I 

              j_id = (jx-1)/nnt

              if(j_id == nid) then
                 xd=zhopv(idw,jdw)*asignv_icd
c                 wv1(i)=wv1(i)+xd*wv2(jx)
                 wv1(i-nid_nnt)=wv1(i-nid_nnt)+xd*wv2(jx-nid_nnt)
              else
                   n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                   n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icd
                   nc_id(j_id) = nc_id(j_id)+1
              end if
201     continue
     
c************************************************************************
       endif 
c************************************************************************


        if(ncount .ne. 0) then          
           allocate(tmp_all(ncount))
          do 202 i=0,nsize-1
                count_test = 0

              if( nid .ne. i) then
                if(ncount_nsize(i) .ne. 0) then
                 allocate(tmp_data(ncount_nsize(i)))
                 do j=1,ncount_nsize(i)
                   jj_icd = abs(n_mesg(i)%n_i(j))
                   tmp_data(j)%tmp = (n_mesg(i)%n_i(j))/jj_icd *
     &                                wv2(jj_icd-nid_nnt)
                    tmp_data(j)%nn_jx = n_mesg(i)%n_jx(j)
                 enddo
           call MPI_ISEND(tmp_data,ncount_nsize(i),newtype,i,
     &             i, MPI_COMM_WORLD,request,ierr)
 
           call MPI_WAIT(request,status,ierr)
                 deallocate(tmp_data)
 
             endif                

              else

                 do 203,while (count_test .lt. ncount) 
          call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,flag,status,ierr)
                      
              if(flag .eq. .true. )then
                    j_id = status(MPI_SOURCE)
                    j_tag = status(MPI_TAG)
                
          call MPI_RECV(tmp_all,ncount_nsize(j_id),newtype,j_id,
     &           j_tag,MPI_COMM_WORLD,status,ierr)

                 j_size = ncount_nsize(j_id)
              do jj = 1,j_size

c                    wv1(tmp_all(jj)%nn_jx) = wv1(tmp_all(jj)%nn_jx)+
c     &              hoptq*tmp_all(jj)%tmp
        wv1(tmp_all(jj)%nn_jx-nid_nnt) = wv1(tmp_all(jj)%nn_jx-nid_nnt)+
     &              hoptq*tmp_all(jj)%tmp
        
                 enddo
              count_test = count_test +j_size
              endif 
203              continue
             end if
202      continue
        
        deallocate(tmp_all)

        end if

c        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
            if(ncount .ne. 0) then
              do i=0,nsize-1
                 if(ncount_nsize(i) .ne. 0)then
                         deallocate( n_mesg(i)%n_jx )
                         deallocate( n_mesg(i)%n_i )
                 endif
             enddo
            endif
             
c*************************Spin up*****************************
           do i=0,nsize-1
             ncount_nsize(i)=0  
             nc_id(i)=0
           enddo
           ncount = 0

c************************************************************
        if(nnt .ge. ndimu) then
c************************************************************
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 215 indexu = 1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 215
               
              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn=ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)
              
              do indexd = indexd_s,indexd_t
                 jx=iavn+indexd*ndimu

                 j_id = (jx-1)/nnt

                 if(j_id .ne. nid ) then
                    ncount = ncount+1
                    ncount_nsize(j_id) = ncount_nsize(j_id)+1
                 endif 
                 
              enddo
  215     continue 


c***********************************************************
        else
c***********************************************************
           do 205 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu
              
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 205

              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn=ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)
C
              jx=iavn+indexd*ndimu

              j_id = (jx-1)/nnt

              if(j_id .ne. nid ) then 
                 ncount = ncount+1
                 ncount_nsize(j_id) = ncount_nsize(j_id)+1
              endif

205        continue
c*********************************************************
        endif 
c*********************************************************

         
         if(ncount .ne. 0) then
              do i=0,nsize-1
                 if(ncount_nsize(i) .ne. 0)then
                         allocate( n_mesg(i)%n_jx(ncount_nsize(i)) )
                         allocate( n_mesg(i)%n_i(ncount_nsize(i)) )
                         nc_id(i) = 1
                 endif
              enddo
            endif
        
c********************************************************
        if(nnt .ge. ndimu) then
c********************************************************
              indexd_s = (nid*nnt)/ndimu
              indexd_t = ((nid+1)*nnt-1)/ndimu

          do 216 indexu = 1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 216

              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn=ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)

              if(icget .ne. 0) then
                 icu=iand(icget,ncodeu)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icu,2)
                    icu= icu/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icu = 1.0
                 else
                    asignv_icu = -1.0
                 endif 
              else
                    asignv_icu = 1.0
              endif 


              do indexd = indexd_s,indexd_t
                 jx=iavn+indexd*ndimu

                 j_id = (jx-1)/nnt
                 i = indexd*ndimu+indexu

                 if(j_id == nid) then
                    xu=zhopv(iup,jup)*asignv_icu
c                    wv1(i)=wv1(i)+xu*wv2(jx)
                    wv1(i-nid_nnt)=wv1(i-nid_nnt)+xu*wv2(jx-nid_nnt)
                 else
                    n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                    n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icu
                    nc_id(j_id) = nc_id(j_id)+1
                 endif
              enddo
  216     continue
        
c********************************************************
        else
c********************************************************
           do 206 i = nid*nnt+1,(nid+1)*nnt
              indexd = (i-1)/ndimu
              indexu = i-indexd*ndimu
              
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iaget,ncodeu)/iaget
              jup=iand(ibget,ncodeu)/ibget
              if(iup .eq. jup) goto 206

              icodeu=ncodeu+(jup-iup)*nfac
c              iavn=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavn=ia
              iavn = find_index(ndimui,mcodeu(1),icodeu)

c              icu=iand(icget,ncodeu)
c              xu=zhopv(iup,jup)*asignv(icu)
C
              if(icget .ne. 0) then
                 icu=iand(icget,ncodeu)
                 mu = 0
                 do j=1,nsites
                    mu = mu + mod(icu,2)
                    icu= icu/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    asignv_icu = 1.0
                 else
                    asignv_icu = -1.0
                 endif 
              else
                    asignv_icu = 1.0
              endif 

              jx=iavn+indexd*ndimu

              j_id = (jx-1)/nnt

              if(j_id == nid) then
                 xu=zhopv(iup,jup)*asignv_icu
c                 wv1(i)=wv1(i)+xu*wv2(jx)
                 wv1(i-nid_nnt)=wv1(i-nid_nnt)+xu*wv2(jx-nid_nnt)
              else
                    n_mesg(j_id)%n_jx(nc_id(j_id)) = jx
                    n_mesg(j_id)%n_i(nc_id(j_id)) = i*asignv_icu
                    nc_id(j_id) = nc_id(j_id)+1
              end if
206        continue
c***************************************************************
        endif 
c***************************************************************
       

          if(ncount .ne. 0) then
             allocate(tmp_all(ncount))
          do 207 i=0,nsize-1
                count_test=0
              if( nid .ne. i) then
                if(ncount_nsize(i) .ne. 0) then
                 allocate(tmp_data(ncount_nsize(i)))
                 do j=1,ncount_nsize(i)
                   jj_icd = abs(n_mesg(i)%n_i(j))
                   tmp_data(j)%tmp = (n_mesg(i)%n_i(j))/jj_icd *
     &                                wv2(jj_icd-nid_nnt)
                    tmp_data(j)%nn_jx = n_mesg(i)%n_jx(j)
                 enddo
           call MPI_ISEND(tmp_data,ncount_nsize(i),newtype,i,
     &             i, MPI_COMM_WORLD,request,ierr)
 
           call MPI_WAIT(request,status,ierr)
                 deallocate(tmp_data)
 
             endif
                
              else

                 do 208 ,while (count_test .lt. ncount)
          call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,flag,status,ierr)
                      
                      if(flag .eq. .true.) then
                    j_id = status(MPI_SOURCE)
                    j_tag = status(MPI_TAG)

             call MPI_RECV(tmp_all,ncount_nsize(j_id),newtype,j_id,
     &           j_tag,MPI_COMM_WORLD,status,ierr)

             j_size = ncount_nsize(j_id)
                 do jj = 1,j_size
c                       wv1(tmp_all(jj)%nn_jx) =wv1(tmp_all(jj)%nn_jx)+
c     &                 hoptq*tmp_all(jj)%tmp
        wv1(tmp_all(jj)%nn_jx-nid_nnt) =wv1(tmp_all(jj)%nn_jx-nid_nnt)+
     &                 hoptq*tmp_all(jj)%tmp
                 enddo             
                count_test = count_test + j_size
              endif 
208              continue
             end if
207      continue
         deallocate(tmp_all)         
         endif

         call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        if(ncount .ne. 0) then
              do i=0,nsize-1
                 if(ncount_nsize(i) .ne. 0)then
                         deallocate( n_mesg(i)%n_jx )
                         deallocate( n_mesg(i)%n_i )
                 endif
              enddo
            endif

        endif

1001    continue


        return
        end


C Set Initial Electron Configuration

        subroutine initl(init,nid,wv1)
        include 'uvj.h'
        integer nid,ii
        integer*8 i_dex
        real*8  wv1(1:nnt)
        integer*8 flag1,flag2
        integer*8 ,external :: find_index

        flag1 = 1
        flag2 = 2

        initx=init/1000
        inity=mod(init,1000)
        if(initx .ge. 1) goto 9500

        if(nid == 0) then  
           write(6,1002) (i,i=1,nsites)
        endif

        anor=0.0E0
        do 9000 intime=1,inity

191     continue

        if(nid == 0) then 
        read(5,*) x,(itmpv(i),i=1,nsites)
        endif


         call MPI_BCAST(x,1,MPI_REAL8,0,MPI_COMM_WORLD,ierr)
         call MPI_BCAST(itmpv,nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)


        do 5 i=1,nsites
           nv(i,1)=0
           nv(i,2)=0
           if(itmpv(i) .eq. 1) nv(i,1)=1
           if(itmpv(i) .eq.-1) nv(i,2)=1
           if(itmpv(i) .eq. 2) nv(i,1)=1
           if(itmpv(i) .eq. 2) nv(i,2)=1
5       continue
	if(dabs(x) .le. 1.0E-8) then
           if(nid .eq. 0) then
           call ranini
           endif 
           x=1.0
        end if
        call MPI_BCAST(itmpv,nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
       call MPI_BCAST(nv(1,1),nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)
       call MPI_BCAST(nv(1,2),nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)

C                                               Checking
        nupx=0
        ndwx=0
        do 10 i=1,nsites
           nupx=nupx+nv(i,1)
           ndwx=ndwx+nv(i,2)
10      continue
        if(nupx.ne.nup .or. ndwx.ne.ndown) then
           write(6,*) 'Wrong in Spin #'
           go to 191
        end if
        
        call code(nv,nsites,flag1,ncodeu)
        call code(nv,nsites,flag2,ncoded)

c        i=iav(ncodeu)+ibv(ncoded)
c        do ib =1, ndimdi
c           if( mcoded(ib-1) .eq. ncoded ) exit
c        enddo
c        ibvn = (ib-1)*ndimui

c        do ia=1,ndimui
c           if( mcodeu(ia) .eq. ncodeu) exit
c        enddo
c        iavn = ia
        ibvn = (find_index(ndimdi,mcoded(0),ncoded)-1)*ndimui
        iavn = find_index(ndimui,mcodeu(1),ncodeu)

        i=iavn + ibvn

C       get the nrank of the index i
        ii = (i-1)/nnt
        i_dex = i-ii*nnt


        nwzi(intime)=i

        if(ii .eq. nid ) then
            wv1(i_dex)=x
        endif

        anor=anor+x*x

        
        if(nid == 0) then
c            write(6,1004) intime,x,ncodeu,(nv(j,1),j=1,nsites)
c            write(6,1006) i,ncoded,(nv(j,2),j=1,nsites)
            write(6,1008) intime,x,ncodeu,ncoded,(itmpv(j),j=1,nsites)
        end if

9000    continue

9500    continue
        if(initx .eq. 0) return

        if(nid == 0) then 
            do 9550 intime=1,inity
               read(5,*) x,(itmpv(i),i=1,nsites)
9550        continue
        endif

        call MPI_BCAST(x,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(itmpv,nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)

1002    format(21x,'S:     X',9x,36i2)
1004    format(1x,i3,'th Comp. ',f7.4,2x,'U: ',i8,1x,36i2)
1006    format(14x,i8,2x,'D: ',i8,1x,36i2)
1008    format(1x,i2,'_Comp. ',f7.4,2x,'U_',i8,' D_',i8,2x,36i2)

        return
        end


        subroutine ranini
        include "uvj.h"

        scale=1.0/(2.0**31-1.0)
        iseed=time()
        if(iseedx .ne. 0) iseed=iseedx
        iseedx=iseed
        do j=1,nsites
           nv(j,1)=0
           nv(j,2)=0
        end do

        itmp=0
10      continue
        iseed=mod(16807*iseed, 2147483167)
        xxx=scale*iabs(iseed)
        j=nsites*xxx+1
        if(nv(j,1) .eq. 0 .and. itmp .lt. nup) then
           nv(j,1)=1
           itmp=itmp+1
        end if
        if (itmp .lt. nup) goto 10

        itmp=0
20      continue
        iseed=mod(16807*iseed, 2147483167)
        xxx=scale*iabs(iseed)
        j=nsites*xxx+1
        if(nv(j,2) .eq. 0 .and. itmp .lt. ndown) then
           nv(j,2)=1
           itmp=itmp+1
        end if
        if (itmp .lt. ndown) goto 20

        do j=1,nsites
           itmpv(j)=0
           if(nv(j,1) .eq. 1) itmpv(j)=1
           if(nv(j,2) .eq. 1) itmpv(j)=-1
           if(nv(j,1)*nv(j,2) .eq. 1) itmpv(j)=2
        end do
      
        return
        end


C Standard Lanczos Method

        subroutine lancs(nlanc,jp,nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 tmp
        real*8 wv1(1:nnt)
        real*8 wv2(1:nnt)
        real*8 wz(1:nnt)


        
        small=1.0E-7
        jobn=0
        n1=1
        n2=2
        betav(1)=zero

c       nlanc = 196         


        do 1000 j=1,nlanc

           if(nid == 0) then
           call second(tt5)
           endif 

           mox=mod(j,10)
           if(nid == 0) then
              open(unit=2,file=swv(mox),status='unknown')
           end if
           jp1=j+1

           alpha=zero
c           do 110 m=nid*nnt+1,(nid+1)*nnt
           do 110 m=1,nnt
              alpha=alpha+wv1(m)*wv2(m)
110        continue
           
           call MPI_ALLREDUCE(alpha,tmp1,1,MPI_REAL8,MPI_SUM,
     &                MPI_COMM_WORLD,ierr)
       
           alpha = tmp1
           alphav(j)=alpha


           if(n1 .eq. 1) then
c           do 120 m=nid*nnt+1,(nid+1)*nnt
           do 120 m=1,nnt
              wv2(m)=wv2(m)-alpha*wv1(m)
120        continue
           else if(n1 .eq. 2) then
c           do 220 m=nid*nnt+1,(nid+1)*nnt
           do 220 m=1,nnt
              wv1(m)=wv1(m)-alpha*wv2(m)
220        continue
           end if

           fbeta=zero
           if(n1 .eq. 1) then
c           do 130 m=nid*nnt+1,(nid+1)*nnt
           do 130 m=1,nnt
              fbeta=fbeta+wv2(m)*wv2(m)
130        continue
           else if(n1 .eq. 2) then
c           do 230 m=nid*nnt+1,(nid+1)*nnt
           do 230 m=1,nnt
              fbeta=fbeta+wv1(m)*wv1(m)
230        continue
           end if

         call MPI_ALLREDUCE(fbeta,tmp2,1,MPI_REAL8,MPI_SUM,
     &                 MPI_COMM_WORLD,ierr)

          
           fbeta = tmp2
           beta=dsqrt(fbeta)
           betav(jp1)=beta
           gamma=beta
           
           if(nid == 0) then 
               write(6,1012) j,alpha,beta
               write(2,1012) j,alpha,beta
1012       format(1x,'Step',i3,2x,'alpha =',f22.16,2x,'beta =',f22.16)
           end if


           if(gamma .lt. small) then
              if(nid == 0) then
                  write(6,*) 'Complete  gamma =',gamma
              endif 
              goto 199
           end if
           if(j.eq.nlanc) goto 1000
           if(j.lt.3) goto 299

C                                               Eigenvalues

           call eigen(j,jobn,wz)

           if(nid == 0) then
               write(6,1014) (d(i),i=1,3)
               write(2,1014) (d(i),i=1,3)
           end if
           do 160 i=1,3
              de(i)=d(i)-de(i)
160        continue
           dtest=de(1)
           if(j.gt.3)  then
               if(nid == 0) then
                  write(6,1016) (de(i),i=1,3)
                  write(2,1016) (de(i),i=1,3)
               endif
           end if
1014    format(5x,'E = ',2(f20.14,1x),f20.14)
1016    format(4x,'dE = ',2(f20.14,1x),f20.14)
            

           if(dabs( dtest ) .lt. 1.0E-12)  goto 199

           do 180 i=1,j
              de(i)=d(i)
180        continue

299        continue

           if(n1 .eq. 1) then
c           do 145 m=nid*nnt+1,(nid+1)*nnt
           do 145 m=1,nnt
              wv2(m)=wv2(m)/gamma  ! wv2 = q
145        continue
           else if(n1 .eq. 2) then
c           do 245 m=nid*nnt+1,(nid+1)*nnt
           do 245 m=1,nnt
              wv1(m)=wv1(m)/gamma  
245        continue
           end if

           if(n1 .eq. 1) then
c           do 150 m=nid*nnt+1,(nid+1)*nnt
           do 150 m=1,nnt
              wv1(m)=-beta*wv1(m) ! wv1 = b
150        continue
           else if(n1 .eq. 2) then
c           do 250 m=nid*nnt+1,(nid+1)*nnt
           do 250 m=1,nnt
              wv2(m)=-beta*wv2(m)
250        continue
           end if

              n3=n2
              n2=n1
              n1=n3

           if(j .lt. nlanc) then
                
c           write(6,*) " the first time enter hop in lancs"
              call hop(n1,n2,nid,wv1(1),wv2(1))
           end if

           if(jp1.gt.ndim) then
              if(nid == 0) then
              write(6,*) 'Trouble! Not Complete'
              endif
           end if

           if(nid == 0) then
           call second(tt6)
           endif

           if(nid == 0)then
           write(2,*) j,' Lancs. CPU = ',tt6-tt5
           endif

           if(nid == 0) then            
              if(j .ne. nlanc) close (unit=2)
           endif
1000    continue

199     jp=jp1-1
        
        

        if(nid == 0) then 
           write(6,1001)
           write(2,1001)
        end if
1001    format(1H )
        if(nid == 0) then
           write(6,*) 'Tridiagonal Matrix: Dimension = ',jp
           write(6,1001)
           write(2,*) 'Tridiagonal Matrix: Dimension = ',jp
           write(2,1001)
        
           do 400 i=1,jp
              write(6,1008) i,betav(i),alphav(i),betav(i+1)
              write(2,1008) i,betav(i),alphav(i),betav(i+1)
400     continue
           write(6,1001)
           write(2,1001)
        end if
1008    format(2x,i3,3x,f16.8,2x,f16.8,2x,f16.8)

        see=dabs(alphav(jp))+dabs(betav(jp))
        if(see .lt. small) jp=jp-1

        jobn=1
        call eigen(jp,jobn,wz)

        do 420 i=1,jp
           dx=d(i)-de(i)
           if(dabs( dtest ) .lt. 1.0E-12) dx=de(i)
           if(nid == 0) then
              write(6,1018) i,d(i),dx
              write(2,1018) i,d(i),dx
           end if
420     continue
1018    format(1x,i3,' th E =',f20.14,2x,'dE =',f20.14)
        if(dabs( d(2)-d(1) ) .lt. 1.0E-8) write(6,*) 'LEVEL MIXING?'

        
        if(nid == 0) then
            close (unit=2)
        end if

        return
        end


C Calculate Eigenvalues

        subroutine eigen(ndiv,jobn,wz)
        include 'uvj.h'
        real*8 wz(*)


        do 100 i=1,ndiv
        do 100 j=1,ndiv
           trih(j,i)=zero
100     continue
        do 120 i=1,ndiv
           trih(i,i)=alphav(i)
120     continue
        do 140 i=2,ndiv
           im1=i-1
           trih(i,im1)=betav(i)
           trih(im1,i)=betav(i)
140     continue
       
        
        call tred2(iz,ndiv,trih,d,wz,z)
        call imtql2(iz,ndiv,d,wz,z,ier)
        if(ier.ne.0) write(6,*) 'ier =',ier

        return
        end


C Calculate Eigenvector by Standard Lanczos

        subroutine lancz(nlanc,jp,nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1(1:nnt)
        real*8 wv2(1:nnt)
        real*8 wz(1:nnt)

        n1=1
        n2=2
        

              call hop(n1,n2,nid,wv1(1),wv2(1))

        do 1000 j=1,nlanc

           
           if(nid == 0) call second(tt1)
           if(mod(j,2) .eq. 1 ) then
              if(nid == 0) then
              open (unit=2,file='cza',status='unknown')
              endif 
           else if(mod(j,2) .eq. 0) then
              if(nid == 0) then
              open (unit=2,file='czb',status='unknown')
              endif
           end if

           jp1=j+1
C                                               Eigenvector
           fbeta=z(j,iorx)
           if(n1 .eq. 1) then
c           do 100 m=nid*nnt+1,(nid+1)*nnt
           do 100 m=1,nnt
              wz(m)=wz(m)+fbeta*wv1(m)
100        continue
           else if(n1 .eq. 2) then
c           do 200 m=nid*nnt+1,(nid+1)*nnt
           do 200 m=1,nnt
              wz(m)=wz(m)+fbeta*wv2(m)
200        continue
           end if


           alpha=alphav(j)
           beta=betav(jp1)
           gamma=betav(jp1)
  
           if(gamma.lt.1.0E-7) goto 399

           if(n1 .eq. 1) then
c           do 120 m=nid*nnt+1,(nid+1)*nnt
           do 120 m=1,nnt
              wv2(m)=wv2(m)-alpha*wv1(m)
120        continue
           else if(n1 .eq. 2) then
c           do 220 m=nid*nnt+1,(nid+1)*nnt
           do 220 m=1,nnt
              wv1(m)=wv1(m)-alpha*wv2(m)
220        continue
           end if


           if(n1 .eq. 1) then
c           do 145 m=nid*nnt+1,(nid+1)*nnt
           do 145 m=1,nnt
              wv2(m)=wv2(m)/gamma
145        continue
           else if(n1 .eq. 2) then
c           do 245 m=nid*nnt+1,(nid+1)*nnt
           do 245 m=1,nnt
              wv1(m)=wv1(m)/gamma
245        continue
           end if

           
           if(n1 .eq. 1) then
c           do 150 m=nid*nnt+1,(nid+1)*nnt
           do 150 m=1,nnt
              wv1(m)=-beta*wv1(m)
150        continue
           else if(n1 .eq. 2) then
c           do 250 m=nid*nnt+1,(ni+1)*nnt
           do 250 m=1,nnt
              wv2(m)=-beta*wv2(m)
250        continue
           end if

              n3=n2
              n2=n1
              n1=n3
           if(j .ne. nlanc) then
              call hop(n1,n2,nid,wv1(1),wv2(1))
           endif
      
           if(nid == 0) then
              call second(tt2)
              write(2,*) 'Lancz. Step ',j,'  CPU = ',tt2-tt1
              close (unit=2)
           endif 
1000    continue

399     return
        end


        subroutine szsz(nid,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 tmp
        real*8 wv2(1:nnt)
        real*8 wz(1:nnt)

C  Correlations                   Corv(i,j,1) = < N(i,1)N(j,1) >
C                                 Corv(i,j,2) = < N(i,2)N(j,2) >
C                                 Corv(i,j,3) = < N(i,1)N(j,2) >
C                                 Corv(i,j,4) = <Hole(i)Hole(j)>
C  All (i,j) pairs temparary

        anor=zero
c        do 1110 m=nid*nnt+1,(nid+1)*nnt
        do 1110 m=1,nnt
           wv2(m)=wz(m)*wz(m)
           anor=anor+wv2(m)
1110    continue

        call MPI_ALLREDUCE(anor,tmp,1,MPI_REAL8,MPI_SUM,
     &               MPI_COMM_WORLD,ierr)
      
        anor = tmp

        do 10 iup=0,1
        do 10 jup=0,1
           tmpva(iup,jup)=dfloat( iup*jup )
           tmpvb(iup,jup)=dfloat( (1-iup)*(1-jup) )
10      continue
        do 20 iup=0,1
        do 20 idw=0,1
        do 20 jup=0,1
        do 20 jdw=0,1
           tmpvc(iup,idw,jup,jdw)=dfloat( iup*jdw+idw*jup )
20      continue

        do 1200 i=1,nsites
        do 1200 j=1,i
           iget=idenv(i)
           jget=idenv(j)

           do 1201 k=nid*nnt+1,(nid+1)*nnt
              indexd = (k-1)/ndimu
              indexu = k-indexd*ndimu

              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
              tmpdd=tmpva(idw,jdw)
              tmpx=tmpvb(idw,jdw)

              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              jup=iand(jget,ncodeu)/jget
              tmpuu=tmpva(iup,jup)
              tmpud=tmpvc(iup,idw,jup,jdw)
                 
c              wvm2=wv2(k)
              wvm2=wv2(k-nid*nnt)
              corv(i,j,1)=corv(i,j,1)+wvm2*tmpuu
              corv(i,j,2)=corv(i,j,2)+wvm2*tmpdd
              corv(i,j,3)=corv(i,j,3)+wvm2*tmpud
              corv(i,j,4)=corv(i,j,4)+wvm2*tmpvb(iup,jup)*tmpx
1201      continue            

          do kk = 1,4
           call MPI_ALLREDUCE(corv(i,j,kk),tmp,1,MPI_REAL8,MPI_SUM,
     &               MPI_COMM_WORLD,ierr)
           corv(i,j,kk) = tmp

c           if(nid == 0) then
c             write(6,*) "corv",corv(i,j,kk)
c           endif
          enddo 

1200    continue            

        return
        end


        subroutine sxsx(nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        integer*8 , external :: find_index
        
        type send_data
              real*8   tmp
              integer*8  nn_jx
c              integer*8  nn_i
        end type
 
        type n_send
             integer*8,allocatable::n_jx(:)
c             integer*8,allocatable::n_j_id(:)
             integer*8,allocatable::n_i(:)
        end type

        integer*8 ncount,num_recv
        real*8 tmp
        integer*8,allocatable::ncount_nsize(:),nc_id(:) 

        real*8 wv1(1:nnt)
        real*8 wv2(1:nnt)
        real*8 wz(1:nnt)
        integer status(MPI_STATUS_SIZE),request
        LOGICAL flag
        type(send_data),allocatable::tmp_data(:),tmp_all(:)
        type(n_send),allocatable::n_mesg(:)
        integer Block(2),Disp(2),Tp(2),newtype

C                                 Scorx(i,j)  = < S(i,+)S(j,-) + hc >

C                                   <D(i,+)C(j,+)> and <D(i,-)C(j,-)>


        Block(1)=1
        Disp(1) =0
        Tp(1)   =MPI_REAL8

        Block(2)=1
        Disp(2) =8
        Tp(2)   =MPI_INTEGER8

        call MPI_TYPE_STRUCT(2,Block,Disp,Tp,newtype,ierr)
        call MPI_TYPE_COMMIT(newtype,ierr)

        allocate(ncount_nsize(0:(nsize-1)),nc_id(0:(nsize-1)))
        allocate(n_mesg(0:(nsize-1)))



        do 1300 i=1,nsites
        do 1300 j=1,i
           iget=idenv(i)
           jget=idenv(j)
           icget=nsign1(i,j)
           nfac=iget-jget

c           do 1310 m=nid*nnt+1,(nid+1)*nnt
           do 1310 m=1,nnt
              wv1(m)=zero
              wv2(m)=zero
1310       continue

C                                               wv2 = D(j,-)c(i,-)|0>
c*********************************************************
           do ii=0,nsize-1
                ncount_nsize(ii)=0
                nc_id(ii)=0
           enddo
           num_recv = 0
           ncount = 0

           do 1311 k = nid*nnt+1,(nid+1)*nnt
              indexd = (k-1)/ndimu
              indexu = k-indexd*ndimu

              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
c              if(idw .le. jdw) goto 1311
              if(idw .eq. jdw) goto 1311
              
              icoded=ncoded+(jdw-idw)*nfac
c              ibvd=ibv(icoded)
c              do ib =1, ndimdi
c                 if( mcoded(ib-1) .eq. icoded ) exit
c              enddo
c              ibvd = (ib-1)*ndimui
              ibvd = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui
              
              jxd = indexu+ibvd
              j_id = (jxd-1)/nnt

              if(idw .lt. jdw) then  
                 if(j_id .ne. nid) then
                    ncount_nsize(j_id) = ncount_nsize(j_id)+1
                    ncount = ncount+1
                 endif 
              endif 
              if(idw .gt. jdw) then  
                 if(j_id .ne. nid) then
                    num_recv = num_recv + 1
                    ncount = ncount+1
                 endif 
              endif 
1311       continue

              
             do ii=0,nsize-1
                if(ncount_nsize(ii) .ne. 0)then
                        allocate( n_mesg(ii)%n_jx(ncount_nsize(ii)) )
c                        allocate( n_mesg(ii)%n_j_id(ncount_nsize(ii)))
                        allocate( n_mesg(ii)%n_i(ncount_nsize(ii)) )
                        nc_id(ii) = 1
                endif
             enddo

           do 1312 k = nid*nnt+1,(nid+1)*nnt
              indexd = (k-1)/ndimu
              indexu = k-indexd*ndimu

              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
              if(idw .eq. jdw) goto 1312

                icoded=ncoded+(jdw-idw)*nfac
c                ibvd=ibv(icoded)
c                do ib =1, ndimdi
c                   if( mcoded(ib-1) .eq. icoded ) exit
c                end do
c                ibvd = (ib-1)*ndimui
              ibvd = (find_index(ndimdi,mcoded(0),icoded)-1)*ndimui
                icd=iand(icget,ncoded)

c                xd=asignv(icd)
              
                 mu = 0
                 do jj=1,nsites
                    mu = mu + mod(icd,2)
                    icd= icd/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    xd = 1.0
                 else
                    xd = -1.0
                 endif 


                jxd = indexu+ibvd
                j_id = (jxd-1)/nnt
        
                if(idw .gt. jdw) then
                   if(j_id == nid) then
                  wv2(k-nid*nnt)=wv2(k-nid*nnt)+xd*wz(jxd-nid*nnt)
                   endif 
                endif

                if(idw .lt. jdw)then
                   if(j_id .ne. nid) then
                        n_mesg(j_id)%n_jx(nc_id(j_id)) = jxd
c                        n_mesg(j_id)%n_j_id(nc_id(j_id)) = j_id
                        n_mesg(j_id)%n_i(nc_id(j_id)) = k*xd
   
                        nc_id(j_id) = nc_id(j_id)+1
                   endif
                endif

1312       continue


         if(ncount .ne. 0) then
                allocate(tmp_all(num_recv))
          do 1313 ii=0,nsize-1
                count_test = 0
             if( nid .ne. ii) then
                if(ncount_nsize(ii) .ne. 0) then 
                allocate(tmp_data(ncount_nsize(ii)))
                   do jj=1,ncount_nsize(ii)
                      jj_icd = abs(n_mesg(ii)%n_i(jj))
                      tmp_data(jj)%tmp   = (n_mesg(ii)%n_i(jj))/jj_icd*
     &                                      wz(jj_icd-nid*nnt) 
                      tmp_data(jj)%nn_jx = n_mesg(ii)%n_jx(jj)
                   enddo
           call MPI_ISEND(tmp_data,ncount_nsize(ii),newtype,ii,
     &             ncount_nsize(ii), MPI_COMM_WORLD,request,ierr)

          call MPI_WAIT(request,status,ierr)
                deallocate(tmp_data)
                endif 

             else

         do 1314 ,while (count_test .lt. num_recv)
           call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,flag,status,ierr)

                if(flag == .true. ) then

                    j_id = status(MPI_SOURCE)
                    j_tag = status(MPI_TAG)
          call MPI_RECV(tmp_all,num_recv,newtype,j_id,
     &           j_tag,MPI_COMM_WORLD,status,ierr)

                 j_size = j_tag
                 do jj = 1,j_size
c                    indexd = (tmp_all(jj)%nn_jx-1)/ndimu
c                    ncoded=mcoded(indexd)
c                    icd=iand(icget,ncoded)
c                    xd=asignv(icd)

        wv2(tmp_all(jj)%nn_jx-nid*nnt) =wv2(tmp_all(jj)%nn_jx-nid*nnt)+
     &               tmp_all(jj)%tmp
                enddo
                count_test = count_test +j_size
               endif
1314      continue
 
             end if
1313      continue
         
           deallocate(tmp_all)

         end if  ! endif ncount .ne. 0


        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
              do ii=0,nsize-1
                 if(ncount_nsize(ii) .ne. 0)then
                         deallocate( n_mesg(ii)%n_jx )
c                         deallocate( n_mesg(ii)%n_j_id )
                         deallocate( n_mesg(ii)%n_i )
                 endif
              enddo

c***************************************************************

         do ii=0,nsize-1
               ncount_nsize(ii)=0
               nc_id(ii)=0
         enddo
         num_recv = 0
         ncount = 0

C                                               <D(j,+)C(i,+)>
           do 1316 k = nid*nnt+1,(nid+1)*nnt
              indexd = (k-1)/ndimu
              indexu = k-indexd*ndimu

              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              jup=iand(jget,ncodeu)/jget
              icodeu=ncodeu+(jup-iup)*nfac
c              iavu=iav(icodeu)
c              do ia=1,ndimui
c                 if( mcodeu(ia) .eq. icodeu) exit
c              enddo
c              iavu = ia
              iavu = find_index(ndimui,mcodeu(0),icodeu)

c              if(iup .le. jup) goto 1316
              if(iup .eq. jup) goto 1316
              
              
              jxu = iavu+indexd*ndimu
              j_id = (jxu-1)/nnt

              if(iup .lt. jup) then
                 if(j_id .ne. nid) then
                    ncount_nsize(j_id) = ncount_nsize(j_id)+1
                    ncount = ncount+1
                 endif
              endif
              if(iup .gt. jup) then
                 if(j_id .ne. nid) then
                    num_recv = num_recv + 1
                    ncount = ncount+1
                 endif
              endif
        
1316       continue


           do ii=0,nsize-1
                if(ncount_nsize(ii) .ne. 0)then
                        allocate( n_mesg(ii)%n_jx(ncount_nsize(ii)))
c                        allocate( n_mesg(ii)%n_j_id(ncount_nsize(ii)))
                        allocate( n_mesg(ii)%n_i(ncount_nsize(ii)))
                        nc_id(ii) = 1
                endif
           enddo


           do 1317 k = nid*nnt+1,(nid+1)*nnt
              indexd = (k-1)/ndimu
              indexu = k-indexd*ndimu

              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              jup=iand(jget,ncodeu)/jget
              if(iup .eq. jup) goto 1317

                 icodeu=ncodeu+(jup-iup)*nfac
c                 iavu=iav(icodeu)
c                 do ia=1,ndimui
c                    if( mcodeu(ia) .eq. icodeu) exit
c                 enddo
c                 iavu = ia
              iavu = find_index(ndimui,mcodeu(0),icodeu)

                 icu=iand(icget,ncodeu)
c                 xu=asignv(icu)
              
                 mu = 0
                 do jj=1,nsites
                    mu = mu + mod(icu,2)
                    icu= icu/2
                 enddo
                 mu = mod(mu,2)
                 if(mu .eq. 0) then
                    xu = 1.0
                 else
                    xu = -1.0
                 endif 

                 jxu = iavu + indexd*ndimu
                 j_id = (jxu-1)/nnt

                 if(iup .gt. jup) then
                    if(j_id .eq. nid) then
                wv1(k-nid*nnt)=wv1(k-nid*nnt)+xu*wz(jxu-nid*nnt)
                    endif 
                 endif

                 if(iup .lt. jup)then
                    if(j_id .ne. nid) then
                         n_mesg(j_id)%n_jx(nc_id(j_id)) = jxu
c                         n_mesg(j_id)%n_j_id(nc_id(j_id)) = j_id
                         n_mesg(j_id)%n_i(nc_id(j_id)) = k*xu
 
                         nc_id(j_id) = nc_id(j_id)+1
                    endif
                 endif
1317       continue


         if(ncount .ne. 0) then
                allocate(tmp_all(num_recv))

         do 1318 ii=0,nsize-1
 
              count_test=0

              if( nid .ne. ii) then
                  if(ncount_nsize(ii) .ne. 0) then
                  allocate(tmp_data(ncount_nsize(ii)))
                    do jj=1,ncount_nsize(ii)
                      jj_icd = abs(n_mesg(ii)%n_i(jj))
                      tmp_data(jj)%tmp   = (n_mesg(ii)%n_i(jj))/jj_icd*
     &                                      wz(jj_icd-nid*nnt) 
                       tmp_data(jj)%nn_jx = n_mesg(ii)%n_jx(jj)
                    enddo
            call MPI_ISEND(tmp_data,ncount_nsize(ii),newtype,ii,
     &             ncount_nsize(ii), MPI_COMM_WORLD,request,ierr)
 
            call MPI_WAIT(request,status,ierr)
                 deallocate(tmp_data) 
                
                 endif        
         
              else

                do 1319 ,while (count_test .lt. num_recv)
            call MPI_IPROBE(MPI_ANY_SOURCE,MPI_ANY_TAG,
     &                    MPI_COMM_WORLD,flag,status,ierr)

                 if(flag == .true. ) then
 
                     j_id = status(MPI_SOURCE)
                     j_tag = status(MPI_TAG)
           call MPI_RECV(tmp_all,num_recv,newtype,j_id,
     &           j_tag,MPI_COMM_WORLD,status,ierr)
 
                  j_size = j_tag
                  do jj = 1,j_size
c                     indexd = (tmp_all(jj)%nn_jx-1)/ndimu
c                     indexu = tmp_all(jj)%nn_jx-indexd*ndimu
c                     ncodeu=mcoded(indexu)
c                     icu=iand(icget,ncodeu)
c                     xu=asignv(icu)
 
        wv1(tmp_all(jj)%nn_jx-nid*nnt)=wv1(tmp_all(jj)%nn_jx-nid*nnt)+
     &               tmp_all(jj)%tmp
                 enddo
                 count_test = count_test +j_size
                endif
 1319      continue

           endif 
        
 1318      continue ! end ii=1,nsize-1
         
        deallocate(tmp_all)

        end if

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
              do ii=0,nsize-1
                 if(ncount_nsize(ii) .ne. 0)then
                         deallocate( n_mesg(ii)%n_jx )
c                         deallocate( n_mesg(ii)%n_j_id )
                         deallocate( n_mesg(ii)%n_i )
                 endif
              enddo

c           do 1360 m=nid*nnt+1,(nid+1)*nnt
           do 1360 m=1,nnt
              corv(i,j,5)=corv(i,j,5)+wv1(m)*wz(m)
              corv(i,j,6)=corv(i,j,6)+wv2(m)*wz(m)
1360       continue
           
          do kk = 5,6
           call MPI_ALLREDUCE(corv(i,j,kk),tmp,1,MPI_REAL8,MPI_SUM,
     &               MPI_COMM_WORLD,ierr)
           corv(i,j,kk) = tmp
          enddo 

c           do 1370 m=nid*nnt+1,(nid+1)*nnt
           do 1370 m=1,nnt
              scorx(i,j)=scorx(i,j)-wv2(m)*wv1(m)
1370       continue

           call MPI_ALLREDUCE(scorx(i,j),tmp,1,MPI_REAL8,MPI_SUM,
     &               MPI_COMM_WORLD,ierr)
           scorx(i,j) = tmp

1300    continue

        return
        end


C  Bond-Order
        subroutine bxybxy(nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1((nid*nnt+1):((nid+1)*nnt))
        real*8 wv2((nid*nnt+1):((nid+1)*nnt))
        real*8 wz((nid*nnt+1):((nid+1)*nnt))

C X-X

        do 3100 i=1,nsites

C                             WV1 = Sum_s [ D(i,s)c(i+x,s) + h.c ] | G >
           do 3110 m=1,ndim
              wv1(m)=zero
3110       continue

           ipx=ipxv(i)
           iget=idenv(i)
           ipxget=idenv(ipx)
           nfacix=iget-ipxget
           icxget=nsign1(i,ipx)

	   apbcx=1.0E0
           if(ibc .eq. 1) then
              if(i .eq. nsites .and. ipx .eq. 1) apbcx=-1.0E0
           end if
C                                               Spin Up
           do 3120 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              ipxup=iand(ipxget,ncodeu)/ipxget
              if(iup .eq. ipxup) goto 3120
              icu=iand(icxget,ncodeu)
              nfaciu=(ipxup-iup)*nfacix

c              iavu=iav(ncodeu+nfaciu)
                
              ! by me 
!!              xu=apbcx*asignv(icu)

              do 3140 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jx=iavu+ibvd
                 wv1(m)=wv1(m)+xu*wz(jx)
3140          continue

3120       continue

C                                               Spin Down
           do 3160 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              ipxdw=iand(ipxget,ncoded)/ipxget
              if(idw .eq. ipxdw) goto 3160
              icd=iand(icxget,ncoded)
              nfacid=(ipxdw-idw)*nfacix
c              ibvd=ibv(ncoded+nfacid)

              ! by me 
!!              xd=apbcx*asignv(icd)
              madd=indexd*ndimu

              do 3180 indexu=1,ndimu
                 m=indexu+madd
                 jx=indexu+ibvd
                 wv1(m)=wv1(m)+xd*wz(jx)
3180          continue

3160       continue

           do 3200 j=1,i

C                             WV2 = Sum_s [ D(j,s)c(j+x,s) + h.c ] | G >
           do 3210 m=1,ndim
              wv2(m)=zero
3210       continue

           jpx=ipxv(j)
           jget=idenv(j)
           jpxget=idenv(jpx)
           nfacjx=jget-jpxget
           jcxget=nsign1(j,jpx)

	   apbcy=1.0E0
           if(ibc .eq. 1) then
              if(j .eq. nsites .and. jpx .eq. 1) apbcy=-1.0E0
           end if
C                                               Spin Up
           do 3220 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              jup=iand(jget,ncodeu)/jget
              jpxup=iand(jpxget,ncodeu)/jpxget
              if(jup .eq. jpxup) goto 3220
              jcu=iand(jcxget,ncodeu)
              nfacju=(jpxup-jup)*nfacjx
c              iavu=iav(ncodeu+nfacju)
        
              ! by me 
!!              xu=apbcy*asignv(jcu)

              do 3240 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jx=iavu+ibvd
                 wv2(m)=wv2(m)+xu*wz(jx)
3240          continue

3220       continue

C                                               Spin Down
           do 3260 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              jdw=iand(jget,ncoded)/jget
              jpxdw=iand(jpxget,ncoded)/jpxget
              if(jdw .eq. jpxdw) goto 3260
              jcd=iand(jcxget,ncoded)
              nfacjd=(jpxdw-jdw)*nfacjx
c              ibvd=ibv(ncoded+nfacjd)

              ! by me 
!!              xd=apbcy*asignv(jcd)
              madd=indexd*ndimu

              do 3280 indexu=1,ndimu
                 m=indexu+madd
                 jx=indexu+ibvd
                 wv2(m)=wv2(m)+xd*wz(jx)
3280          continue

3260       continue

           do 3190 m=1,ndim
              bowv(i,j,1)=bowv(i,j,1)+wv1(m)*wv2(m)
3190       continue

3200       continue

3100    continue

C Y-Y:  Note, No Anti-Periodic Here Yet!
	if(nd12 .eq. 1) return

        do 3300 i=1,nsites
C                           WV1 = [ D(i,+)C(i+y,+) + D(i,-)C(i+y,-) ] |G>
           do 3310 m=1,ndim
              wv1(m)=zero
3310       continue

           ipy=ipyv(i)
           iget=idenv(i)
           ipyget=idenv(ipy)
           nfaciy=iget-ipyget
           icyget=nsign1(i,ipy)
C                                               Spin Up
           do 3320 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              ipyup=iand(ipyget,ncodeu)/ipyget
              if(iup .eq. ipyup) goto 3320
              icu=iand(icyget,ncodeu)
              nfaciu=(ipyup-iup)*nfaciy
c              iavu=iav(ncodeu+nfaciu)

              ! by me 
!!              yu=asignv(icu)

              do 3340 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jx=iavu+ibvd
                 wv1(m)=wv1(m)+yu*wz(jx)
3340          continue

3320       continue

C                                               Spin Down
           do 3360 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              ipydw=iand(ipyget,ncoded)/ipyget
              if(idw .eq. ipydw) goto 3360
              icd=iand(icyget,ncoded)
              nfacid=(ipydw-idw)*nfaciy
c              ibvd=ibv(ncoded+nfacid)
        
              ! by me
!!              yd=asignv(icd)
              madd=indexd*ndimu

              do 3380 indexu=1,ndimu
                 m=indexu+madd
                 jx=indexu+ibvd
                 wv1(m)=wv1(m)+yd*wz(jx)
3380          continue

3360       continue

           do 3400 j=1,i
C                           WV2 = [ D(j,+)C(j+y,+) + D(j,-)C(j+y,-) ] |G>
           do 3410 m=1,ndim
              wv2(m)=zero
3410       continue

           jpy=ipyv(j)
           jget=idenv(j)
           jpyget=idenv(jpy)
           nfacjy=jget-jpyget
           jcyget=nsign1(j,jpy)
C                                               Spin Up
           do 3420 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              jup=iand(jget,ncodeu)/jget
              jpyup=iand(jpyget,ncodeu)/jpyget
              if(jup .eq. jpyup) goto 3420
              jcu=iand(jcyget,ncodeu)
              nfacju=(jpyup-jup)*nfacjy
c              iavu=iav(ncodeu+nfacju)

              ! by me
!!              yu=asignv(jcu)

              do 3440 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jx=iavu+ibvd
                 wv2(m)=wv2(m)+yu*wz(jx)
3440          continue

3420       continue

C                                               Spin Down
           do 3460 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              jdw=iand(jget,ncoded)/jget
              jpydw=iand(jpyget,ncoded)/jpyget
              if(jdw .eq. jpydw) goto 3460
              jcd=iand(jcyget,ncoded)
              nfacjd=(jpydw-jdw)*nfacjy
c              ibvd=ibv(ncoded+nfacjd)

              ! by me
!!              yd=asignv(jcd)

              madd=indexd*ndimu

              do 3480 indexu=1,ndimu
                 m=indexu+madd
                 jx=indexu+ibvd
                 wv2(m)=wv2(m)+yd*wz(jx)
3480          continue

3460       continue

           do 3390 m=1,ndim
              bowv(i,j,2)=bowv(i,j,2)+wv1(m)*wv2(m)
3390       continue

3400       continue

3300    continue

        return
        end


C  Current-Current Correlations
        subroutine jxjx(nid,wv1)
        include 'uvj.h'
        integer*8 nid
        real*8 wv1((nid*nnt+1):((nid+1)*nnt))
        real*8 wv2((nid*nnt+1):((nid+1)*nnt))
        real*8 wz((nid*nnt+1):((nid+1)*nnt))

        do 5100 i=1,nsites

C                             WV1 = Sum_s [ D(i,s)c(i+x,s) - h.c ] | G >
           do 5110 m=1,ndim
              wv1(m)=zero
5110       continue

           ipx=ipxv(i)
           iget=idenv(i)
           ipxget=idenv(ipx)
           nfacix=iget-ipxget
           icxget=nsign1(i,ipx)

	   apbcx=1.0E0
           if(ibc .eq. 1) then
              if(i .eq. nsites .and. ipx .eq. 1) apbcx=-1.0E0
           end if
C                                               Spin Up
           do 5120 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              ipxup=iand(ipxget,ncodeu)/ipxget
              if(iup .eq. ipxup) goto 5120
              icu=iand(icxget,ncodeu)
              nfaciu=(ipxup-iup)*nfacix
c              iavu=iav(ncodeu+nfaciu)

              ! by me 
!!              xu=apbcx*dfloat(iup-ipxup)*asignv(icu)

              do 5140 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jx=iavu+ibvd
                 wv1(m)=wv1(m)+xu*wz(jx)
5140          continue

5120       continue

C                                               Spin Down
           do 5160 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              ipxdw=iand(ipxget,ncoded)/ipxget
              if(idw .eq. ipxdw) goto 5160
              icd=iand(icxget,ncoded)
              nfacid=(ipxdw-idw)*nfacix
c              ibvd=ibv(ncoded+nfacid)

              ! by me 
!!              xd=apbcx*dfloat(idw-ipxdw)*asignv(icd)
              madd=indexd*ndimu

              do 5180 indexu=1,ndimu
                 m=indexu+madd
                 jx=indexu+ibvd
                 wv1(m)=wv1(m)+xd*wz(jx)
5180          continue

5160       continue

           do 5200 j=1,i

C                             WV2 = Sum_s [ D(j,s)c(j+x,s) - h.c ] | G >
           do 5210 m=1,ndim
              wv2(m)=zero
5210       continue

           jpx=ipxv(j)
           jget=idenv(j)
           jpxget=idenv(jpx)
           nfacjx=jget-jpxget
           jcxget=nsign1(j,jpx)

	   apbcy=1.0E0
           if(ibc .eq. 1) then
              if(j .eq. nsites .and. jpx .eq. 1) apbcy=-1.0E0
           end if
C                                               Spin Up
           do 5220 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              jup=iand(jget,ncodeu)/jget
              jpxup=iand(jpxget,ncodeu)/jpxget
              if(jup .eq. jpxup) goto 5220
              jcu=iand(jcxget,ncodeu)
              nfacju=(jpxup-jup)*nfacjx
c              iavu=iav(ncodeu+nfacju)

! by me 
!!              xu=apbcy*dfloat(jup-jpxup)*asignv(jcu)

              do 5240 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jx=iavu+ibvd
                 wv2(m)=wv2(m)+xu*wz(jx)
5240          continue

5220       continue

C                                               Spin Down
           do 5260 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              jdw=iand(jget,ncoded)/jget
              jpxdw=iand(jpxget,ncoded)/jpxget
              if(jdw .eq. jpxdw) goto 5260
              jcd=iand(jcxget,ncoded)
              nfacjd=(jpxdw-jdw)*nfacjx
c              ibvd=ibv(ncoded+nfacjd)

              ! by me 
!!              xd=apbcy*dfloat(jdw-jpxdw)*asignv(jcd)
              madd=indexd*ndimu

              do 5280 indexu=1,ndimu
                 m=indexu+madd
                 jx=indexu+ibvd
                 wv2(m)=wv2(m)+xd*wz(jx)
5280          continue

5260       continue

           do 5190 m=1,ndim
              curv(i,j)=curv(i,j)+wv1(m)*wv2(m)
5190       continue

5200       continue

5100    continue

        return
        end


        subroutine pxpx(nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1((nid*nnt+1):((nid+1)*nnt))
        real*8 wv2((nid*nnt+1):((nid+1)*nnt))
        real*8 wz((nid*nnt+1):((nid+1)*nnt))


C P(l,k) = (1/N) sum{i,j} < c(i+l,+)c(i,-) D(j,-)D(j+k,+) >
C        = (1/N) sum{i,j} < c(i,-)D(j,-) c(i+l,+)D(j+k,+) >
C (wv2, wv1) = < c(i,-)D(j,-) c(i+l,+)D(j+k,+) >
C Note, c(j)D(i)|0> and c(i+l)D(j+k) |0> has negative sign in program!
C All (l,k) pairs temparary


        do 1120 k=1,nsites
        do 1120 l=1,nsites
	   pair(l,k)=zero
1120    continue

	do 1200 l=1,nsites
	do 1200 k=1,l

	lxcord=ixv(l)
        lycord=iyv(l)
        kxcord=ixv(k)
        kycord=iyv(k)

	sum=zero
        do 1300 i=1,nsites
        do 1300 j=1,nsites

           do 1310 m=1,ndim
              wv1(m)=zero
              wv2(m)=zero
1310       continue

C               wv2(m) = c(j,-)D(i,-)|0> = - D(i,-)c(j,-) |0>  if i ne j
C                                        = [ 1 - n(i,-) ] |0>  if i eq j
           iget=idenv(i)
           jget=idenv(j)
           icget=nsign1(i,j)
           nfac=iget-jget

	if(i .ne. j) then

           do 1320 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
              if(idw .ge. jdw) goto 1320
              icoded=ncoded+(jdw-idw)*nfac
c              ibvd=ibv(icoded)
              madd=indexd*ndimu
              icd=iand(icget,ncoded)

              ! by me
!!              xd=asignv(icd)

              do 1340 indexu=1,ndimu
                 m=indexu+madd
                 jxd=indexu+ibvd
                 wv2(m)=wv2(m)-xd*wz(jxd)
1340          continue

1320       continue

	else if(i .eq. j) then

	   do 1420 indexd=0,ndimdm
	      ncoded=mcoded(indexd)
c	      ncoded=ncvec(indexd+1)
	      idw=iand(iget,ncoded)/iget
c	      xd=dfloat(idw)
	      if(idw .eq. 1) goto 1420
	      madd=indexd*ndimu
	      do 1440 indexu=1,ndimu
	         m=indexu+madd
	         wv2(m)=wv2(m)+wz(m)
1440          continue
1420       continue

	end if

C                                       ipl = i+l; jpk = j+k.
	iplx=mod( ixv(i)+lxcord, lxs )
        iply=mod( iyv(i)+lycord, lys )
        ipl=iposit(iplx,iply)
        jpkx=mod( ixv(j)+kxcord, lxs )
        jpky=mod( iyv(j)+kycord, lys )
        jpk=iposit(jpkx,jpky)

 	apbc=1.0E0
	if(ibc .eq. 1) then             ! Anti-Periodic
 	   ansite=dfloat(nsites)-0.5
 	   if(ixv(i)+lxcord .gt. ansite) apbc=-apbc
 	   if(ixv(j)+kxcord .gt. ansite) apbc=-apbc
	end if
C Note: ipl = jpk  DOES NOT mean they both passed boundary!

c          ipl=ipair(i,l)
c          jpk=ipair(j,k)
C wv1(m) = c(i+l,+)D(j+k,+)|0> = - D(j+k,+)c(i+l,+) |0>  if i+l ne j+k
C                              = [ 1 - n(i+l,+) ]   |0>  if i+l eq j+k
	   iget=idenv(ipl)
	   jget=idenv(jpk)
	   icget=nsign1(ipl,jpk)
	   nfac=iget-jget

	if(ipl .ne. jpk) then

           do 1330 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              jup=iand(jget,ncodeu)/jget
              if(iup .le. jup) goto 1330
              icodeu=ncodeu+(jup-iup)*nfac
c              iavu=iav(icodeu)
              icu=iand(icget,ncodeu)
c             xu=asignv(icu)

              ! by me
!!              xu=apbc*asignv(icu)

              do 1350 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jxu=iavu+ibvd
                 wv1(m)=wv1(m)-xu*wz(jxu)
1350          continue

1330       continue

	else if(ipl .eq. jpk) then

	   do 1430 indexu=1,ndimu
	      ncodeu=mcodeu(indexu)
c	      ncodeu=ncvec(indexu)
	      iup=iand(iget,ncodeu)/iget
	      if(iup .eq. 1) goto 1430
c	      xu=dfloat(iup)
	      do 1450 indexd=0,ndimdm
	         ibvd=indexd*ndimu
		 m=indexu+ibvd
 		 wv1(m)=wv1(m)+apbc*wz(m)
1450          continue
1430       continue

	end if

           do 1370 m=1,ndim
              sum=sum+wv2(m)*wv1(m)
1370       continue

1300    continue

	   pair(l,k)=sum/dfloat(nsites)

1200    continue

	do 1140 l=2,nsites
	do 1140 k=1,l-1
	   pair(k,l)=pair(l,k)
1140    continue

        return
        end


        subroutine pxpx_h(nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1((nid*nnt+1):((nid+1)*nnt))
        real*8 wv2((nid*nnt+1):((nid+1)*nnt))
        real*8 wz((nid*nnt+1):((nid+1)*nnt))

C This is conjugate of pxpx.

C P(l,k) = (1/N) sum{i,j} < D(i,-)D(i+l,+) c(j+k,+)c(j,-) >
C        = (1/N) sum{i,j} < D(i,-)c(j,-) D(i+l,+)c(j+k,+) >
C (wv2, wv1) = ( <0|D(i,-)c(j,-) , D(i+l,+)c(j+k,+)|0> )
C All (l,k) pairs temparary


        do 1120 k=1,nsites
        do 1120 l=1,nsites
	   pair(l,k)=zero
1120    continue

	do 1200 l=1,nsites
	do 1200 k=1,l

	lxcord=ixv(l)
        lycord=iyv(l)
        kxcord=ixv(k)
        kycord=iyv(k)

	sum=zero
        do 1300 i=1,nsites
        do 1300 j=1,nsites

           do 1310 m=1,ndim
              wv1(m)=zero
              wv2(m)=zero
1310       continue

C       				wv2^+ = D(j,-)c(i,-)|0>
           iget=idenv(i)
           jget=idenv(j)
           icget=nsign1(i,j)
           nfac=iget-jget

	if(i .ne. j) then

           do 1320 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
              if(idw .le. jdw) goto 1320
              icoded=ncoded+(jdw-idw)*nfac
c              ibvd=ibv(icoded)
              madd=indexd*ndimu
              icd=iand(icget,ncoded)

              ! by me
!!              xd=asignv(icd)

              do 1340 indexu=1,ndimu
                 m=indexu+madd
                 jxd=indexu+ibvd
                 wv2(m)=wv2(m)+xd*wz(jxd)
1340          continue

1320       continue

	else if(i .eq. j) then

	   do 1420 indexd=0,ndimdm
	      ncoded=mcoded(indexd)
c	      ncoded=ncvec(indexd+1)
	      idw=iand(iget,ncoded)/iget
c	      xd=dfloat(idw)
	      if(idw .eq. 0) goto 1420
	      madd=indexd*ndimu
	      do 1440 indexu=1,ndimu
	         m=indexu+madd
	         wv2(m)=wv2(m)+wz(m)
1440          continue
1420       continue

	end if

C                                       ipl = i+l; jpk = j+k.
	iplx=mod( ixv(i)+lxcord, lxs )
        iply=mod( iyv(i)+lycord, lys )
        ipl=iposit(iplx,iply)
        jpkx=mod( ixv(j)+kxcord, lxs )
        jpky=mod( iyv(j)+kycord, lys )
        jpk=iposit(jpkx,jpky)

	apbc=1.0E0
        if(ibc .eq. 1) then             ! Anti-Periodic
           ansite=dfloat(nsites)-0.5
           if(ixv(i)+lxcord .gt. ansite) apbc=-apbc
           if(ixv(j)+kxcord .gt. ansite) apbc=-apbc
        end if

C                                       wv1 = D(i+l,+)c(j+k,+)|0>
	   iget=idenv(ipl)
	   jget=idenv(jpk)
	   icget=nsign1(ipl,jpk)
	   nfac=iget-jget

	if(ipl .ne. jpk) then

           do 1330 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              iup=iand(iget,ncodeu)/iget
              jup=iand(jget,ncodeu)/jget
              if(iup .ge. jup) goto 1330
              icodeu=ncodeu+(jup-iup)*nfac
c              iavu=iav(icodeu)
              icu=iand(icget,ncodeu)
c             xu=asignv(icu)

              ! by me
!!              xu=apbc*asignv(icu)

              do 1350 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jxu=iavu+ibvd
                 wv1(m)=wv1(m)+xu*wz(jxu)
1350          continue

1330       continue

	else if(ipl .eq. jpk) then

	   do 1430 indexu=1,ndimu
	      ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
	      iup=iand(iget,ncodeu)/iget
	      if(iup .eq. 0) goto 1430
c	      xu=dfloat(iup)
	      do 1450 indexd=0,ndimdm
	         ibvd=indexd*ndimu
		 m=indexu+ibvd
	         wv1(m)=wv1(m)+apbc*wz(m)
1450          continue
1430       continue

	end if

           do 1370 m=1,ndim
              sum=sum+wv2(m)*wv1(m)
1370       continue

1300    continue

	   pair(l,k)=sum/dfloat(nsites)

1200    continue

	do 1140 l=2,nsites
	do 1140 k=1,l-1
	   pair(k,l)=pair(l,k)
1140    continue

        return
        end


        subroutine pijkl(nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1((nid*nnt+1):((nid+1)*nnt))
        real*8 wv2((nid*nnt+1):((nid+1)*nnt))
        real*8 wz((nid*nnt+1):((nid+1)*nnt))

C This is different from pxpx!

C P(i,j,k,l) = < c(l,+)c(i,-) D(j,-)D(k,+) > (Different from spinless fermion!)
C            = < c(i,-)D(j,-) c(l,+)D(k,+) >
C            = <0| c(i,-)D(j,-) c(l,+)D(k,+) |0> = (wv2, wv1)
C Note, c(j)D(i)|0> and c(l)D(k) |0> has negative sign compare to before!
C All (l,k) pairs temparary

	do 1200 l=1,nsites
	do 1200 k=1,nsites

           do 1220 m=1,ndim
              wv1(m)=zero
1220       continue

C                                       wv1(m) = c(l,+)D(k,+) |0>
	   kget=idenv(k)
	   lget=idenv(l)
	   icget=nsign1(l,k)
	   nfac=kget-lget

	if(l .ne. k) then

           do 1330 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              kup=iand(kget,ncodeu)/kget
              lup=iand(lget,ncodeu)/lget
              if(kup .ge. lup) goto 1330
              icodeu=ncodeu+(lup-kup)*nfac
c              iavu=iav(icodeu)
              icu=iand(icget,ncodeu)

              ! by me 
!!              xu=asignv(icu)

              do 1350 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jxu=iavu+ibvd
                 wv1(jxu)=wv1(jxu)-xu*wz(m)
1350          continue

1330       continue

	else if(l .eq. k) then

	   do 1430 indexu=1,ndimu
	      ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
	      kup=iand(kget,ncodeu)/kget
	      if(kup .eq. 1) goto 1430
c	      xu=dfloat(kup)
	      do 1450 indexd=0,ndimdm
	         ibvd=indexd*ndimu
		 m=indexu+ibvd
		 wv1(m)=wv1(m)+wz(m)
1450          continue
1430       continue

	end if

        do 1300 i=1,nsites
        do 1300 j=1,nsites

           do 1310 m=1,ndim
              wv2(m)=zero
1310       continue

C       				wv2(m) = c(j,-)D(i,-)|0>
           iget=idenv(i)
           jget=idenv(j)
           icget=nsign1(i,j)
           nfac=iget-jget

	if(i .ne. j) then

           do 1320 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncodeu=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
              if(idw .ge. jdw) goto 1320
              icoded=ncoded+(jdw-idw)*nfac
c              ibvd=ibv(icoded)
              madd=indexd*ndimu
              icd=iand(icget,ncoded)

              ! by me
!!              xd=asignv(icd)


              do 1340 indexu=1,ndimu
                 m=indexu+madd
                 jxd=indexu+ibvd
                 wv2(jxd)=wv2(jxd)-xd*wz(m)
1340          continue

1320       continue

	else if(i .eq. j) then

	   do 1420 indexd=0,ndimdm
	      ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
	      idw=iand(iget,ncoded)/iget
c	      xd=dfloat(1-idw)
	      if(idw .eq. 1) goto 1420
	      madd=indexd*ndimu
	      do 1440 indexu=1,ndimu
	         m=indexu+madd
	         wv2(m)=wv2(m)+wz(m)
1440          continue
1420       continue

	end if

	   sum=zero
           do 1370 m=1,ndim
              sum=sum+wv2(m)*wv1(m)
1370       continue
	   pairm(i,j,k,l)=sum

1300    continue

1200    continue

	open (unit=13,file='fpairm',status='unknown')
        write(13,*) uint(1),vint(1),' = U, V'
        write(13,*) ((((pairm(i,j,k,l),i=1,nsites),
     &                  j=1,nsites),k=1,nsites),l=1,nsites)
        close (unit=13)

        return
        end


        subroutine pijkl_r(nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1((nid*nnt+1):((nid+1)*nnt))
        real*8 wv2((nid*nnt+1):((nid+1)*nnt))
        real*8 wz((nid*nnt+1):((nid+1)*nnt))
	character*8 cijf(1024)

C This is similar to pijkl, with I/O.

C P(i,j,k,l) = < c(l,+)c(i,-) D(j,-)D(k,+) > (Different from spinless fermion!)
C            = < c(i,-)D(j,-) c(l,+)D(k,+) >
C            = <0| c(i,-)D(j,-) c(l,+)D(k,+) |0> = (wv2, wv1)
C Note c(j,-)D(i,-)|0> and c(l,+)D(k,+) |0> has negative sign compare to before!
C All (l,k) pairs temparary

C File Names for I/O
        open (unit=13,file='cijfname',status='old')
        do 10 ij=1,nsites*nsites
           read(13,1301) cijf(ij)
10      continue
1301    format(a8)
        close (unit=13)

C Making c(l,+)D(k,+)|0> and save them on disk


	do 1200 l=1,nsites
	do 1200 k=1,nsites

           do 1220 m=1,ndim
              wv1(m)=zero
1220       continue

C                                       wv1(m) = c(l,+)D(k,+) |0>
	   kget=idenv(k)
	   lget=idenv(l)
	   icget=nsign1(l,k)
	   nfac=kget-lget

	if(l .ne. k) then

           do 1230 indexu=1,ndimu
              ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
              kup=iand(kget,ncodeu)/kget
              lup=iand(lget,ncodeu)/lget
              if(kup .ge. lup) goto 1230
              icodeu=ncodeu+(lup-kup)*nfac
c              iavu=iav(icodeu)
              icu=iand(icget,ncodeu)

              ! by me
!!              xu=asignv(icu)

c             xu=apbc*asignv(icu)

              do 1250 indexd=0,ndimdm
                 ibvd=indexd*ndimu
                 m=indexu+ibvd
                 jxu=iavu+ibvd
                 wv1(jxu)=wv1(jxu)-xu*wz(m)
1250          continue

1230       continue

	else if(l .eq. k) then

	   do 1260 indexu=1,ndimu
	      ncodeu=mcodeu(indexu)
c              ncodeu=ncvec(indexu)
	      kup=iand(kget,ncodeu)/kget
	      if(kup .eq. 1) goto 1260
c	      xu=dfloat(kup)
	      do 1270 indexd=0,ndimdm
	         ibvd=indexd*ndimu
		 m=indexu+ibvd
		 wv1(m)=wv1(m)+wz(m)
c		 wv1(m)=wv1(m)+apbc*wz(m)
1270          continue
1260       continue

	end if

C Write c(l,+)D(k,+)|0> to disk
           itmp=(k-1)*nsites+l
           open(unit=13,file=cijf(itmp),form='unformatted',
     &           status='unknown')
           write(13) (wv1(m),m=1,ndim)
           close(unit=13)

1200    continue

	do 1160 k=1,nsites
        do 1160 l=1,nsites
        do 1160 j=1,nsites
        do 1160 i=1,nsites
           pairm(i,j,l,k)=zero
1160    continue

        do 1300 i=1,nsites
        do 1300 j=1,nsites

           do 1310 m=1,ndim
              wv2(m)=zero
1310       continue

C       				wv2(m) = c(j,-)D(i,-)|0>
           iget=idenv(i)
           jget=idenv(j)
           icget=nsign1(i,j)
           nfac=iget-jget

	if(i .ne. j) then

           do 1320 indexd=0,ndimdm
              ncoded=mcoded(indexd)
c              ncoded=ncvec(indexd+1)
              idw=iand(iget,ncoded)/iget
              jdw=iand(jget,ncoded)/jget
              if(idw .ge. jdw) goto 1320
              icoded=ncoded+(jdw-idw)*nfac
c              ibvd=ibv(icoded)
              madd=indexd*ndimu
              icd=iand(icget,ncoded)

              ! by me
!!              xd=asignv(icd)

              do 1340 indexu=1,ndimu
                 m=indexu+madd
                 jxd=indexu+ibvd
                 wv2(jxd)=wv2(jxd)-xd*wz(m)
1340          continue

1320       continue

	else if(i .eq. j) then

	   do 1350 indexd=0,ndimdm
	      ncoded=mcoded(indexd)
c	      ncoded=ncvec(indexd+1)
	      idw=iand(iget,ncoded)/iget
c	      xd=dfloat(1-idw)
	      if(idw .eq. 1) goto 1350
	      madd=indexd*ndimu
	      do 1360 indexu=1,ndimu
	         m=indexu+madd
	         wv2(m)=wv2(m)+wz(m)
1360          continue
1350       continue

	end if

	do 1420 l=1,nsites
	do 1420 k=1,nsites

C Read in c(l,+)D(k,+)|0> = wv1
           itmp=(k-1)*nsites+l
           open(unit=13,file=cijf(itmp),form='unformatted',
     &          status='unknown')
           read(13) (wv1(m),m=1,ndim)
           close(unit=13)

	   sum=zero
           do 1370 m=1,ndim
              sum=sum+wv2(m)*wv1(m)
1370       continue
	   pairm(i,j,k,l)=sum

1420    continue

1300    continue

	open (unit=13,file='fpairm',status='unknown')
        write(13,*) uint(1),vint(1),' = U, V'
        write(13,*) ((((pairm(i,j,k,l),i=1,nsites),
     &                  j=1,nsites),k=1,nsites),l=1,nsites)
        close (unit=13)

        return
        end


        subroutine frt
        include 'uvj.h'

        do 110 l=1,icor
        do 110 k=1,nsites
           fk(k,l)=czero
110     continue

        do 120 l=1,icor
        do 120 k=1,nsites

           do 140 i=1,nsites
           do 140 j=1,nsites
	      tmpx=akxv(k)*( xv(i)-xv(j) )
     &            +akyv(k)*( yv(i)-yv(j) )
              cxxx=dcmplx( zero,tmpx )
              fk(k,l)=fk(k,l)+cdexp(cxxx)*corv(i,j,l)
140        continue
           fk(k,l)=fk(k,l)/dfloat(nsites)

120     continue

        return
        end


        subroutine gsweig(init,nid,wv1,wv2,wz)
        include 'uvj.h'
        integer nid
        real*8 wv1(1:nnt)
        real*8 wv2(1:nnt)
        real*8 wz(1:nnt)
        real*8 tmp
        integer*8 nhist_tmp(51),flag1,flag2

        flag1 = 1
        flag2 = 2
        if(nid .eq. 0) then
           write(6,1001)
           write(6,*) 'Ground State Wave Function Weights:'
        endif 

        if(init/1000 .ne. 0) goto 1129
        if(nid .eq. 0) then
           write(6,*) 'Initial Weights:              Init=',init
        endif 

        do 1125 intx=1,init
           m=nwzi(intx)
           indexd=(m-1)/ndimu
           indexu=m-ndimu*indexd
           ncodeu=mcodeu(indexu)
           ncoded=mcoded(indexd)
           call decode(nv,nsites,flag1,ncodeu)
           call decode(nv,nsites,flag2,ncoded)
           call plot
           

           if(nid .eq. m/nnt ) then
           write(6,1114) m,wz(m-nid*nnt),mcodeu(indexu),mcoded(indexd),
     &                   (sigmac(j),j=1,nsites)
           endif 

1114    format(1x,i12,2x,f9.6,1x,i12,1x,i12,2x,36a2)
1125    continue

        if(nid .eq. 0)then
        write(6,1001)
        endif 
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        
c********************************************************************
        if(ndim .le. 36) then
            do m=1,ndim
              indexd=(m-1)/ndimu
              indexu=m-ndimu*indexd
              ncodeu=mcodeu(indexu)
              ncoded=mcoded(indexd)
              call decode(nv,nsites,flag1,ncodeu)
              call decode(nv,nsites,flag2,ncoded)
              call plot
              
              if(nid .eq. 0)then
c              write(6,1114) m,wz(m),mcodeu(indexu),mcoded(indexd),
c     8                   (sigmac(j),j=1,nsites)
              endif 

           end do
           return
        end if

1129    continue

c************************************************************
c        if(0) then
c************************************************************
c       do 1130 m=nid*nnt+1,(nid+1)*nnt
       do 1130 m=1,nnt
           wv2(m)=dabs(wz(m))
1130    continue

        coef1=zero
        coef2=zero
        coef3=zero
        coef4=zero
        small=1.0E-5

c        do 1711 m=nid*nnt+1,(nid+1)*nnt
        do 1711 m=1,nnt
           if(wv2(m).gt.coef1) coef1=wv2(m)
1711    continue
        

        
        call MPI_ALLREDUCE(coef1,tmp,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD
     &                     ,ierr)
        coef1 = tmp

c        do 1712 m=nid*nnt+1,(nid+1)*nnt
        do 1712 m=1,nnt
           if(wv2(m).lt.coef1-small.and.wv2(m).gt.coef2) then
              coef2=wv2(m)
           end if
1712    continue


        call MPI_ALLREDUCE(coef2,tmp,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD
     &                     ,ierr)
        coef2 = tmp

c        do 1713 m=1,ndim
c           if(wv2(m).lt.coef2-small.and.wv2(m).gt.coef3) then
c              coef3=wv2(m)
c           end if
c1713    continue
c        do 1714 m=1,ndim
c           if(wv2(m).lt.coef3-small.and.wv2(m).gt.coef4) then
c              coef4=wv2(m)
c           end if
c1714    continue

c************************************************************
c        if(0) then
c************************************************************

        max=0
        do 1720 m=nid*nnt+1,(nid+1)*nnt
           if( dabs(wv2(m-nid*nnt)-coef1) .gt. small ) goto 1720
           max=max+1
           indexd=(m-1)/ndimu
           indexu=m-ndimu*indexd
           ncodeu=mcodeu(indexu)
           ncoded=mcoded(indexd)
           call decode(nv,nsites,flag1,ncodeu)
           call decode(nv,nsites,flag2,ncoded)

           call plot
         write(6,1114) m,wz(m-nid*nnt),mcodeu(indexu),mcoded(indexd),
     8                   (sigmac(j),j=1,nsites)
           mset1=m
1720    continue
        
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)

        call MPI_ALLREDUCE(mset1,mp,1,MPI_INTEGER8,MPI_MAX,
     &            MPI_COMM_WORLD,ierr)
        mset1 = mp

        call MPI_ALLREDUCE(max,mp,1,MPI_INTEGER8,MPI_SUM,
     &            MPI_COMM_WORLD,ierr)
        max = mp

        if(nid .eq. 0)  write(6,*) 'Total # = ',max

        max=0
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        do 1740 m=nid*nnt+1,(nid+1)*nnt
           if( dabs(wv2(m-nid*nnt)-coef2) .gt. small ) goto 1740
           max=max+1
           indexd=(m-1)/ndimu
           indexu=m-ndimu*indexd
           ncodeu=mcodeu(indexu)
           ncoded=mcoded(indexd)
           call decode(nv,nsites,flag1,ncodeu)
           call decode(nv,nsites,flag2,ncoded)
           call plot
         write(6,1114) m,wz(m-nid*nnt),mcodeu(indexu),mcoded(indexd),
     8                   (sigmac(j),j=1,nsites)
           mset2=m
1740    continue

        call MPI_ALLREDUCE(mset1,mp,1,MPI_INTEGER8,MPI_MAX,
     &            MPI_COMM_WORLD,ierr)
        mset2 = mp
        call MPI_ALLREDUCE(max,mp,1,MPI_INTEGER8,MPI_SUM,
     &            MPI_COMM_WORLD,ierr)
        max = mp

        if(max .eq. 0) mset2=0  
        if(nid .eq. 0)   write(6,*) 'Total # = ',max

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)

C Searching Zero Components
c**************************************************************
c        if(0) then
c**************************************************************
        open (unit=2,file='wzzero',status='unknown')
        ntotal=0

        do 1745 m=nid*nnt+1,(nid+1)*nnt
           if( wv2(m-nid*nnt) .eq. 0.0 ) then
              indexd=(m-1)/ndimu
              indexu=m-ndimu*indexd
              ncodeu=mcodeu(indexu)
              ncoded=mcoded(indexd)
              call decode(nv,nsites,1,ncodeu)
              call decode(nv,nsites,2,ncoded)
              call plot
              write(2,1115) m,mcodeu(indexu),mcoded(indexd),
     8                      (sigmac(j),j=1,nsites)
        ntotal=ntotal+1
           end if
1745    continue
1115    format(i12,2x,2i12,2x,36a2)
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)

        call MPI_ALLREDUCE(ntotal,mp,1,MPI_INTEGER8,MPI_SUM,
     &            MPI_COMM_WORLD,ierr)
        
        ntotal = mp

        if(nid .eq. 0)then
           write(6,*) 'Total Number of Zero Components = ',ntotal
        endif 
        close (unit=2)

c**************************************************************
c        if(0) then
c**************************************************************

        if(nid .eq. 0) then
           open (unit=2,file='wzhist',status='unknown')
           write(2,2002)
        endif 
        
        dx=0.02E0*coef1
        hdx=0.5E0*dx
        do 1700 index=1,51
           nhist(index)=0
           nhist_tmp(index) = 0
1700    continue

c        do 1702 m=nid*nnt+1,(nid+1)*nnt
        do 1702 m=1,nnt
           index=nint( wv2(m)/dx )+1
           nhist(index)=nhist(index)+1
1702    continue
        
        call MPI_ALLREDUCE(nhist,nhist_tmp,51,MPI_INTEGER8,MPI_SUM,
     &                          MPI_COMM_WORLD,ierr)
        do index = 1,51
           nhist(index) = nhist_tmp(index);
        enddo
        

        xset=0.0E0
        x1=hdx
        ny=0
        ntotal=0
        
        if(nid .eq. 0) then
           write(2,2004) x1,ny
        endif 

        do 1706 index=1,51
           x1=xset-hdx
           x2=xset+hdx
           nhigh=nhist(index)
           ntotal=ntotal+nhigh
           xset=xset+dx
           if(index .eq. 1) goto 1706
           if(nid .eq. 0) then
              write(2,2004) x1,nhigh
              write(2,2004) x2,nhigh
           endif 
1706    continue
        
        if(nid .eq. 0) then
           write(2,2004) x2,ny
           close (unit=2)

           write(6,*) 'Ntotal = ',ntotal
        endif 

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        mgo=0
        do 1708 ih=51,1,-1
           if(mgo .eq. 4) goto 1708
           if(nhist(ih) .ne. 0) then
              mgo=mgo+1
              if(nid .eq. 0) then
                 write(6,*) mgo,'_th Largest Component =',nhist(ih),ih
              endif 
           end if
1708    continue
2002    format('#lt "Wave Function Histogram"')
2004    format(1x,f8.4,i12)

c**************************************************************
c        if(0) then
c**************************************************************
        if(nid .eq. 0 ) then
        write(6,*) 'Checking With Symmetries. Nsym = ',nsym
        endif 

        if(nid .eq. 0) then
            read(5,*) irunx
        endif 

        call MPI_BCAST(irunx,1,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)

        return 

c**************************************************************
c        if(0) then
c**************************************************************
        do 1750 irun=1,irunx
        if(irun .eq. 1) m=mset1
        if(irun .eq. 2) m=mset2
        if(mset2 .eq. 0) goto 1750
        indexd=(m-1)/ndimu
        indexu=m-ndimu*indexd
        ncodeu=mcodeu(indexu)
        ncoded=mcoded(indexd)
        call decode(nv,nsites,flag1,ncodeu)
        call decode(nv,nsites,flag2,ncoded)

        if(irun .gt. 2) then
           if(nid .eq. 0) then
              read(5,*) (itmpv(j),j=1,nsites)
           endif 

        call MPI_BCAST(itmpv,nsites,MPI_INTEGER8,0,MPI_COMM_WORLD,ierr)

           do j=1,nsites
              nv(j,1)=0
              nv(j,2)=0
              if(itmpv(j) .eq. 1) nv(j,1)=1
              if(itmpv(j) .eq.-1) nv(j,2)=1
              if(itmpv(j) .eq. 2) nv(j,1)=1
              if(itmpv(j) .eq. 2) nv(j,2)=1
           end do
        end if

        do 1725 j=1,nsites
           nvs(j,1)=nv(j,1)
           nvs(j,2)=nv(j,2)
1725    continue
        msym=0
        do 1730 isym=1,nsym
           do 1735 j=1,nsites
              nv(isymv(j,isym),1)=nvs(j,1)
              nv(isymv(j,isym),2)=nvs(j,2)
1735       continue
           call code(nv,nsites,flag1,ncodeu)
           call code(nv,nsites,flag2,ncoded)

           do ib =1, ndimdi
              if( mcoded(ib-1) .eq. ncoded ) exit
           enddo
           ibvn = (ib-1)*ndimui

           do ia=1,ndimui
              if( mcodeu(ia) .eq. ncodeu) exit
           enddo
           iavn = ia
           jx = iavn+ibvn
c           indexu=iav(ncodeu)
c           indexd=ibv(ncoded)
c           jx=indexd+indexu

c           if(wv2(jx) .le. 1.0E-7) goto 1730
           if(nid .eq. jx/nnt) then
              if(wv2(jx-nid*nnt) .le. 1.0E-7) goto 1730
                
              msym=msym+1
              call plot
              write(6,1118) isym,jx,wz(jx-nid*nnt),ncodeu,ncoded,
     8                (sigmac(j),j=1,nsites)
              wv2(jx-nid*nnt)=zero
           endif 
1730    continue
        if(nid .eq. 0) then
           write(6,*) '# of Symmetrized States = ',msym
        endif 

1750    continue
1118    format(1x,i3,2x,i12,2x,f9.6,1x,2i12,2x,36a2)

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        if(nid .eq. 0) then
           write(6,1001)
        endif 

1116    format(19x,i8,2x,36i2)
1001    format(1H )

        if(isymch .eq. 1) then
           if(nid .eq. 0) then
              write(6,*) 'Check Symmetries of Wave Function'
           endif 

c        do 1910 m=nid*nnt+1,(nid+1)*nnt
        do 1910 m=1,nnt
           wv1(m)=1.0E0
1910    continue

c        do 1920 m=1,ndim
        do 1920 m=nid*nnt+1,(nid+1)*nnt
           if(dabs(wz(m-nid*nnt)) .lt. 1.0E-8) goto 1920
           if(wv1(m-nid*nnt) .eq. zero) goto 1920
           indexd=(m-1)/ndimu
           indexu=m-ndimu*indexd
           ncodeu=mcodeu(indexu)
           ncoded=mcoded(indexd)
           call decode(nvs,nsites,flag1,ncodeu)
           call decode(nvs,nsites,flag2,ncoded)

           msym=0
           do 1924 isym=1,nsym
              do 1926 j=1,nsites
                 nv(isymv(j,isym),1)=nvs(j,1)
                 nv(isymv(j,isym),2)=nvs(j,2)
1926          continue
              call code(nv,nsites,flag1,ncodeu)
              call code(nv,nsites,flag2,ncoded)
              do ib =1, ndimdi
                 if( mcoded(ib-1) .eq. ncoded ) exit
              enddo
              ibvn = (ib-1)*ndimui

              do ia=1,ndimui
                 if( mcodeu(ia) .eq. ncodeu) exit
              enddo
               iavn = ia
               jx = iavn+ibvn
c              indexu=iav(ncodeu)
c              indexd=ibv(ncoded)

c  jx == m
c              jx=indexd+indexu 
              if(wv1(jx-nid*nnt) .eq. zero) goto 1924
              msym=msym+1
              call plot
              write(6,1118) isym,jx,wz(jx-nid*nnt),ncodeu,ncoded,
     8                 (sigmac(j),j=1,nsites)
              wv1(jx-nid*nnt)=zero
1924       continue

           if(nid .eq. 0) then
              write(6,*) '# of Symmetrized States = ',msym
           endif 
1920    continue
        end if


        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        no=1
        if(no .eq. 1) then
           if(nid .eq. 0) then
              write(6,*) 'No Study Coefficients Matrix'
           endif 
           return
        end if
! Coefficients Matrix; 12/23/2001
        if(nid .eq. 0) then
           write(6,*) ' '
           write(6,*) 'Study Coefficients Matrix'
        endif 

        if(ndimu .gt. 3432) then
           if(nid .eq. 0) then
              write(6,*) 'Ndimu > 3432; Stop!'
           endif 
           return
        end if

        if(ndimu .ne. ndimd) then
           if(nid .eq. 0) then
              write(6,*) 'Ndimu != Ndimd; Stop!'
           endif 
           return
        end if

        do 1960 ncodeu=1,ndimu
        do 1960 ncoded=1,ndimd
           coeffM(ncodeu,ncoded)=zero
1960    continue

        do m=1,ndim
           indexd=(m-1)/ndimu
           indexu=m-ndimu*indexd
           ncodeu=mcodeu(indexu)
           ncoded=mcoded(indexd)
           coeffM(ncodeu,ncoded)=wz(m)
        end do

        call tred2(icoeff,ndimu,coeffM,coeffD,wz,coeffZ)
        call imtql2(icoeff,ndimu,coeffD,wz,coeffZ,ier)
        if(ier.ne.0) write(6,*) 'ier = ',ier

        open(unit=22,file='coeff_M',status='unknown')
        write(22,*) ndimu,ndimd
        do ncodeu=1,ndimu
        write(22,*) coeffD(ncodeu)
        end do
        close (unit=22)

c************************************************************
c        endif 
c************************************************************
        return
        end


C Plotting Electron Configurations ACCORDING to its coordinate
C It is written as sigmac(j=1,..., N), but j != lattice site here.

        subroutine plot
        include 'uvj.h'
	character*6 sigmax(999)

	do j=1,nsites
	   sigmax(j)='o'
           if(nv(j,1) .eq. 1) sigmax(j)='+'
           if(nv(j,2) .eq. 1) sigmax(j)='-'
           if(nv(j,1).eq.1 .and. nv(j,2).eq.1) sigmax(j)='#'
	end do

	if(nd12 .eq. 1) then
	   do j=1,nsites/2
	      sigmac(2*j-1)=sigmax(j)
	      sigmac(2*j  )=sigmax(j+nsites/2)
	   end do
	else
	   do j=1,nsites
	      sigmac(j)=sigmax(j)
	   end do
	end if

        return
        end


        subroutine symbol
        include 'uvj.h'

        swv(1)='cuvj.01'
        swv(2)='cuvj.02'
        swv(3)='cuvj.03'
        swv(4)='cuvj.04'
        swv(5)='cuvj.05'
        swv(6)='cuvj.06'
        swv(7)='cuvj.07'
        swv(8)='cuvj.08'
        swv(9)='cuvj.09'
        swv(0)='cuvj.10'
 
	qwv( 1)='qin00'
	qwv( 2)='qin01'
	qwv( 3)='qin02'
	qwv( 4)='qin03'
	qwv( 5)='qin04'
	qwv( 6)='qin05'
	qwv( 7)='qin06'
	qwv( 8)='qin07'
	qwv( 9)='qin08'
	qwv(10)='qin09'
	qwv(11)='qin10'
	qwv(12)='qin11'
	qwv(13)='qin12'
	qwv(14)='qin13'
	qwv(15)='qin14'
	qwv(16)='qin15'
	qwv(17)='qin16'
	qwv(18)='qin17'
	qwv(19)='qin18'
	qwv(20)='qin19'
	qwv(21)='qin20'
	qwv(22)='qin21'
	qwv(23)='qin22'
	qwv(24)='qin23'
	qwv(25)='qin24'
	qwv(26)='qin25'
	qwv(27)='qin26'
	qwv(28)='qin27'
	qwv(29)='qin28'
	qwv(30)='qin29'
	qwv(31)='qin30'
	qwv(32)='qin31'
	qwv(33)='qin32'
	qwv(34)='qin33'
	qwv(35)='qin34'
	qwv(36)='qin35'

	bclabel(0)='Periodic'
        bclabel(1)='Anti-Periodic'
        bclabel(2)='Open'

	coraw(1)='<N(i,+)N(j,+)>'
        coraw(2)='<N(i,-)N(j,-)>'
        coraw(3)='<N(i,+)N(j,-)>'
        coraw(4)='<Hol(i)Hol(j)>'
        coraw(5)='<D(i,+)C(j,+)>'
        coraw(6)='<D(i,-)C(j,-)>'
        coraw(7)='<S(i,z)S(j,z)>'
        coraw(8)='<S(i,x)S(j,x)>'
        coraw(9)='<B(i,x)B(j,x)>'
        coraw(10)='<B(i,y)B(j,y)>'

        return        
        end


c ... CRAY functions faked by calls to SUN functions     
     
        subroutine second(tcpu)
C FPS   real*8 etime
C SUN   real etime
        real etime
	real*8 tcpu
        real tarray(2)
        tcpu=etime(tarray)
        return
        end


	subroutine date(idate)
        character*24 idate
        call fdate(idate)
        return
        end
C
        SUBROUTINE TRED2(NM,N,A,D,E,Z)
C
        INTEGER*8 I,J,K,L,N, II,NM,JP1
        REAL*8 A(NM,N),D(N),E(N),Z(NM,N)
        REAL*8 F,G,H,HH,SCALE
        REAL*8 DSQRT,DABS,DSIGN
C
        DO 100 I=1,N
C
          DO 100 J = 1,I
              Z(I,J) = A(I,J)
100     CONTINUE
C
        IF (N.EQ. 1) GO TO 320
 
C       ::::::::::::: FOR I = N STEP-1 UNTIL 2.D0:::::::::::
 
        DO 300 II = 2,N
          I = N+2-II
          L = I-1
          H = 0.0D0
          SCALE = 0.0D0
          IF (L.LT.2) GO TO 130
 
C    :::::::::::::: SCALE ROW (ALGOL TOL THEN NOT NEEDED)::::::::::
 
        DO 120 K = 1,L
120     SCALE = SCALE + DABS(Z(I,K))
C
        IF (SCALE.NE.0.0D0) GO TO 140
130     E(I) = Z(I,L)
        GO TO 290
C
140     DO 150 K = 1,L
           Z(I,K) = Z(I,K) / SCALE
           H = H + Z(I,K) * Z(I,K)
150     CONTINUE
C
        F = Z(I,L)
        G = -DSIGN(DSQRT(H),F)
        E(I) = SCALE * G
        H = H -F * G
        Z(I,L) = F - G
        F = 0.0D0
C
          DO 240 J = 1,L
             Z(J,I) = Z(I,J) / H
             G = 0.0D0
 
C  ::::::::::::: FORM ELEMENT OF A*U :::::::::::::::::::::::::
        DO 180 K = 1,J
180     G = G+ Z(J,K) *Z(I,K)
C
        JP1 = J + 1
        IF (L.LT.JP1) GO TO 220
C
        DO 200 K = JP1,L
200     G = G + Z(K,J) * Z(I,K)
 
C  ::::::::::::::::: FORM ELEMENT OF P ::::::::::::::::
 
220     E(J) = G / H
        F = F + E(J) * Z(I,J)
240     CONTINUE
C
        HH = F / (H + H)
 
C  :::::::::::: FORM REDUCED A :::::::::::::
 
        DO 260 J = 1,L
           F = Z(I,J)
           G = E(J) - HH * F
           E(J) = G
C
        DO 260 K = 1,J
           Z(J,K) = Z(J,K) - F * E(K) - G * Z(I,K)
260     CONTINUE
C
290     D(I) = H
300     CONTINUE
C
320     D(1) = 0.0D0
        E(1) = 0.0D0
 
C  ::::::::::::: ACCUMULATION OF TRANSFORMATION MATRICES :::::::::::
 
        DO 500 I = 1,N
           L = I-1
           IF (D(I) .EQ. 0.0D0) GO TO 380
C
           DO 360 J = 1,L
           G = 0.0D0
C
           DO 340 K = 1,L
340        G = G + Z(I,K) * Z(K,J)
C
           DO 360 K = 1,L
           Z(K,J) = Z(K,J) - G * Z(K,I)
360     CONTINUE
C
380     D(I) = Z(I,I)
        Z(I,I) = 1.0D0
        IF (L.LT.1) GO TO 500
C
        DO 400 J = 1,L
           Z(I,J) = 0.0D0
           Z(J,I) = 0.0D0
400     CONTINUE
C
500     CONTINUE
C
        RETURN
 
C       :::::::::::: LAST CARD OF TRED2 ::::::::::::::::
 
        END
C
        SUBROUTINE IMTQL2(NM,N,D,E,Z,IERR)
        INTEGER*8 I,J,K,L,M,N,II,NM,MML,IERR
        REAL*8 D(N),E(N),Z(NM,N)
        REAL*8 B,C,F,G,P,R,S,MACHEP
        REAL*8 DSQRT,DABS,DSIGN
C
        MACHEP = 2.**(-52)
C
        IERR = 0
        IF (N.EQ. 1) GO TO 1001
C
        DO 100 I = 2,N
 
100     E(I-1) = E(I)
C
        E(N) = 0.0D0
C
        DO 240 L = 1,N
           J = 0
 
C       :::::::::::: LOOK FOR SMALL SUB-DIAGONAL ELEMENT :::::::::::
 
105     DO 110 M = L,N
           IF (M .EQ. N) GO TO 120
           IF (DABS(E(M)) .LE. MACHEP*(DABS(D(M)) + DABS(D(M+1))))
     1        GO TO 120
110     CONTINUE
C
120     P = D(L)
        IF (M .EQ. L) GO TO 240
        IF (J .EQ. 30) GO TO 1000
        J = J+1
 
C       :::::::::: FORM SHIFT ::::::::::::::::::::::
 
        G = (D(L+1)-P) / (2.0D0 * E(L))
        R = DSQRT (G*G+1.0D0)
        G = D(M) - P + E(L) / (G + DSIGN(R,G))
        S = 1.0D0
        C = 1.0D0
        P = 0.0D0
        MML = M-L
C       :::::::::::::: FOR I = M-1 STEP -1 UNTIL L DO--::::::::::::
        DO 200 II = 1,MML
           I = M - II
           F = S * E(I)
           B = C * E(I)
           IF  (DABS(F) .LT. DABS(G)) GO TO 150
           C = G / F
           R = DSQRT(C*C+1.0D0)
           E(I+1) = F * R
           S = 1.0D0 / R
           C = C * S
           GO TO 160
150        S = F / G
           R = DSQRT(S*S+1.0D0)
           E(I+1) = G * R
           C = 1.0D0 / R
           S = S * C
160        G = D(I+1) - P
           R = (D(I) - G) * S + 2.0D0 * C * B
           P = S * R
           D(I+1) = G + P
           G = C*R-B
 
C       :::::::::: FORM VECTOR ::::::::::::::::::::::::::::
 
           DO 180 K = 1,N
              F = Z(K,I+1)
              Z(K,I+1) = S * Z(K,I) + C * F
              Z(K,I) = C * Z(K,I) - S * F
180        CONTINUE
C
200     CONTINUE
C
        D(L) = D(L) - P
        E(L) = G
        E(M) = 0.0D0
        GO TO 105
240     CONTINUE
 
C       :::::::::: ORDER EIGENVALUES AND EIGENVECTORS :::::::::::::
 
        DO 300 II = 2,N
           I = II - 1
           K = I
           P = D(I)
C
           DO 260 J = II,N
 
              IF (D(J) .GE. P) GO TO 260
              K = J
              P = D(J)
260        CONTINUE
C
        IF (K .EQ. I) GO TO 300
        D(K) = D(I)
        D(I) = P
 
        DO 280 J = 1,N
           P = Z(J,I)
          Z(J,I) = Z(J,K)
           Z(J,K) = P
280        CONTINUE
C
300     CONTINUE
C
        GO TO 1001
 
C       ::::::: SET ERROR-- NO CONVERGENCE TO AN
C               EIGENVALUE AFTER 30 ITERATIONS ::::::::::::::
1000    IERR = L
1001    RETURN
C       ::::::::: LAST CARD OF SUBROUTINE ::::::
 
        END

        subroutine sort(na,nb,nc,k)
        integer*8 tmp
        integer*8 i,j,k
        integer*8 na(k),nb(k),nc(k)

        
        do i=1,K-1
           do j=i+1,k
              if(nb(j) < nb(i)) then 
                   tmp = nb(j)
                   nb(j) = nb(i)
                   nb(i) = tmp

C   change the nrank id
                   tmp = na(j)
                   na(j) = na(i)
                   na(i) = tmp
                   
                   tmp = nc(j)
                   nc(j) = nc(i)
                   nc(i) = tmp
              endif
           enddo
        enddo

        return 
        end
        
        function find_index(n,mcode,num_index)        
        include 'uvj.h'
        integer*8 n,num_index,left,right,mid
        integer*8 mcode(1:n)
        integer*8 find_index
        

        left = 1
        right= n
        
        do while (left .le. right)
           mid = left+(right-left)/2
           if(mcode(mid) .eq. num_index ) then
                exit
           else
                if( mcode(mid) .gt. num_index ) then
                        right = mid - 1
                else
                        left = mid + 1
                endif
           endif 
        enddo

        find_index = mid
        return 
        end

